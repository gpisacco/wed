require('../../../../../build/styles.min.css');
import {isDebug} from '../../../../fixturesConfig'

export default {
  options: [],
  match_type: "match_canada_calls",
  go_next_type: "go_to_next_node",
  isDebug,
  handleSubmit: (obj)=>{
    console.log(JSON.stringify(obj, null, 2))
  }
}