require('../../../../../build/styles.min.css');
import {isDebug} from '../../../../fixturesConfig'
import numbers from '../../../../data/numbers.json'
import R from 'ramda'

const prepare = R.map(({_id:{$oid}, number})=>({value:$oid, label:number}));
const options = prepare(numbers);

export default {
  number: options[0].value,
  record: true,
  text : 'text',
  options,
  isDebug,
  handleSubmit: (obj)=>{
    console.log(JSON.stringify(obj, null, 2))
  }
}