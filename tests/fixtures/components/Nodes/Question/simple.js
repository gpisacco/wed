import {isDebug} from '../../../../fixturesConfig'

require('../../../../../build/styles.min.css');

import rating_options from '../RatingOptions/simple'
import multiple_choice_options from '../MultipleChoiceOptions/simple'
import audio from '../PlayAudio/simple'

export default {
  options: [],
  text: 'text',
  audio,
  rating_options,
  multiple_choice_options,
  isDebug,
  populateSelect:(obj)=>{console.log('populateSelect')},
  handleSubmit: (obj)=> {
    console.log(JSON.stringify(obj, null, 2))
  }
}