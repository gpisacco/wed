require('../../../../../build/styles.min.css');
import {isDebug} from '../../../../fixturesConfig'

export default {
  type : 'block_call',
  isDebug,
  handleSubmit: (obj)=>{
    console.log(JSON.stringify(obj, null, 2))
  }
}