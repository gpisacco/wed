require('../../../../../build/styles.min.css');
import {isDebug} from '../../../../fixturesConfig'
import campaigns from '../../../../data/campaigns.json'
import R from 'ramda'

const prepare = R.map(({_id, name})=>({value:_id, label:name}));
const options = prepare(campaigns._items);

export default {
  type : options[0].value,
  isDebug,
  options,
  handleSubmit: (obj)=>{
    console.log(JSON.stringify(obj, null, 2))
  }
}