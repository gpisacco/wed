import {isDebug} from '../../../../fixturesConfig'

require('../../../../../build/styles.min.css');

export default {
  isDebug,
  id: '',
  display: 'rating',
  rating: 10,
  handleSubmit: (obj)=>{
    console.log(JSON.stringify(obj, null, 2))
  }
}