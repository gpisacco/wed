import {isDebug} from '../../../../fixturesConfig'

export default {
  isDebug,
  number: 1,
  transition: 'transition',
  label: 'label',
  options: [
    { value: 'one', label: 'One' },
    { value: 'two', label: 'Two' }
  ],
  handleSubmit: (obj)=>{
    console.log(JSON.stringify(obj, null, 2))
  }
}