import {isDebug} from '../../../../fixturesConfig'

require('../../../../../build/styles.min.css');
import audio from '../PlayAudio/simple'

export default {
  audio,
  number: "number",
  text: "text",
  isDebug,
  handleSubmit: (obj)=> {
    console.log(JSON.stringify(obj, null, 2))
  }
}