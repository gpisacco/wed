import {isDebug} from '../../../../fixturesConfig'
import multiple_choice_option from '../MultipleChoiceOption/simple.js'

export default {
  isDebug,
  display: 'multiple_choice',
  id: 1,
  options: [],
  multiple_choice_option,
  populateSelect: (obj)=>{
    console.log('populateSelect');
    console.log(obj)
  },
  handleSubmit: (obj)=>{
    console.log(JSON.stringify(obj, null, 2))
  }
}