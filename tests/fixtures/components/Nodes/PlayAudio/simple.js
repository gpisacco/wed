require('../../../../../build/styles.min.css');
require('../../../../../css/vendor/bootstrap/css/bootstrap.min.css');
require('../../../../../css/vendor/ionicicons/css/ionicicons.css');
import {isDebug} from '../../../../fixturesConfig'

export default {
  src: 'http://cs9-10v4.vk.me/p22/583ee42546974f.mp3',
  isDebug,
  handleSubmit: (obj)=>{
    console.log(JSON.stringify(obj, null, 2))
  }
}