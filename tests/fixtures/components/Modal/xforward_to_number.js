require('../../../../build/styles.min.css');

import question_opts from '../Nodes/Question/simple'
import {isDebug} from '../../../fixturesConfig'

const ForwardToNumber = require('../../../../js/components/Nodes/ForwardToNumber');
const ForwardToNumberFixture = require('../Nodes/ForwardToNumber/simple');

export default {
  modaleComponent:ForwardToNumber,
  componentProps:ForwardToNumberFixture,
  options: [],
  info: {},
  isOpen: true,
  question_opts,
  isDebug,
  toggleModal:(obj)=>{console.log('toggleModal')},
  handleSubmit: (obj)=> {
    console.log(JSON.stringify(obj, null, 2))
  }
}