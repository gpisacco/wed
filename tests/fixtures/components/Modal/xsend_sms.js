require('../../../../build/styles.min.css');

import question_opts from '../Nodes/Question/simple'
import {isDebug} from '../../../fixturesConfig'

const SendSms = require('../../../../js/components/Nodes/SendSms');
const SendSmsFixture = require('../Nodes/SendSms/simple');

export default {
  modaleComponent:SendSms,
  componentProps:SendSmsFixture,
  options: [],
  info: {},
  isOpen: true,
  question_opts,
  isDebug,
  toggleModal:(obj)=>{console.log('toggleModal')},
  handleSubmit: (obj)=> {
    console.log(JSON.stringify(obj, null, 2))
  }
}