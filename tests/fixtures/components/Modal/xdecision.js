require('../../../../build/styles.min.css');

import question_opts from '../Nodes/Question/simple'
import {isDebug} from '../../../fixturesConfig'

const Decision = require('../../../../js/components/Nodes/Decision');
const DecisionFixture = require('../Nodes/Decision/simple');


export default {
  modaleComponent:Decision,
  componentProps:DecisionFixture,
  options: [],
  info: {},
  isOpen: true,
  question_opts,
  isDebug,
  toggleModal:(obj)=>{console.log('toggleModal')},
  handleSubmit: (obj)=> {
    console.log(JSON.stringify(obj, null, 2))
  }
}