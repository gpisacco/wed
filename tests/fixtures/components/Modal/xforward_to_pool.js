require('../../../../build/styles.min.css');

import question_opts from '../Nodes/Question/simple'
import {isDebug} from '../../../fixturesConfig'

const ForwardToPool = require('../../../../js/components/Nodes/ForwardToPool');
const ForwardToPoolFixture = require('../Nodes/ForwardToPool/simple');

export default {
  modaleComponent:ForwardToPool,
  componentProps:ForwardToPoolFixture,
  options: [],
  info: {},
  isOpen: true,
  question_opts,
  isDebug,
  toggleModal:(obj)=>{console.log('toggleModal')},
  handleSubmit: (obj)=> {
    console.log(JSON.stringify(obj, null, 2))
  }
}