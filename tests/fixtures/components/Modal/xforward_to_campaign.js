require('../../../../build/styles.min.css');

import question_opts from '../Nodes/Question/simple'
import {isDebug} from '../../../fixturesConfig'

const ForwardToCampaign = require('../../../../js/components/Nodes/ForwardToCampaign');
const ForwardToCampaignFixture = require('../Nodes/ForwardToCampaign/simple');

export default {
  modaleComponent:ForwardToCampaign,
  componentProps:ForwardToCampaignFixture,
  options: [],
  info: {},
  isOpen: true,
  question_opts,
  isDebug,
  toggleModal:(obj)=>{console.log('toggleModal')},
  handleSubmit: (obj)=> {
    console.log(JSON.stringify(obj, null, 2))
  }
}