require('../../../../build/styles.min.css');

import question_opts from '../Nodes/Question/simple'
import {isDebug} from '../../../fixturesConfig'

const Kill = require('../../../../js/components/Nodes/Kill');
const KillFixture = require('../Nodes/Kill/simple');

export default {
  modaleComponent:Kill,
  componentProps:KillFixture,
  options: [],
  info: {},
  isOpen: true,
  question_opts,
  isDebug,
  toggleModal:(obj)=>{console.log('toggleModal')},
  handleSubmit: (obj)=> {
    console.log(JSON.stringify(obj, null, 2))
  }
}