require('../../../../build/styles.min.css');

import question_opts from '../Nodes/Question/simple'
import {isDebug} from '../../../fixturesConfig'

const Question = require('../../../../js/components/Nodes/Question');
const QuestionFixture = require('../Nodes/Question/simple');

export default {
  modaleComponent:Question,
  componentProps:QuestionFixture,
  options: [],
  info: {},
  isOpen: true,
  question_opts,
  isDebug,
  toggleModal:(obj)=>{console.log('toggleModal')},
  handleSubmit: (obj)=> {
    console.log(JSON.stringify(obj, null, 2))
  }
}