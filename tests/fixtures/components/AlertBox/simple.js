var AlertTypes = require('../../../../js/constants/AlertTypes');
require('../../../../build/styles.min.css');

export default {
  level: AlertTypes.INFO,
  visible: true,
  msg: "Super msg!"
}