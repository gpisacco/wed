require('../../../../build/styles.min.css');

import question_opts from '../Nodes/Question/simple'
import {isDebug} from '../../../fixturesConfig'

export default {
  options: [],
  info : {},
  isOpen:true,
  question_opts,
  isDebug,
  handleSubmit: (obj)=> {
    console.log(JSON.stringify(obj, null, 2))
  }
}