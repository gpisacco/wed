import {generateTests} from 'cosmos-mocha';

const fixturesDir = '../fixtures';
const componentsDir = '../../js';

generateTests(require('react/addons'), __dirname, fixturesDir, componentsDir);
