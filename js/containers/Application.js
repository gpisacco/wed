const React = require('react');
const Reflux = require('reflux');
const PureRenderMixin = require('react/addons').addons.PureRenderMixin;
const { DragDropContext } = require('react-dnd');
const HTML5Backend = require('react-dnd/modules/backends/HTML5');
const SurveyActions = require('../actions/SurveyActions');
const SurveyStore = require('../stores/SurveyStore');

const TreeView = require('./dnd/TreeView');
const Toolbox = require('./dnd/Toolbox');
const AlertBox = require('../components/AlertBox');
const Question = require('../components/Nodes/Question');
const Kill = require('../components/Nodes/Kill');
const SendSms = require('../components/Nodes/SendSms');
const Decision = require('../components/Nodes/Decision');
const PlayAudio = require('../components/Nodes/PlayAudio');
const ForwardToCampaign = require('../components/Nodes/ForwardToCampaign');
const ForwardToNumber = require('../components/Nodes/ForwardToNumber');
const ForwardToPool = require('../components/Nodes/ForwardToPool');
const Modal = require('../components/Modal');
const Pallet = require('./dnd/Pallet');
const LoadSurveyModal = require('../components/LoadSurveyModal');

const Application = React.createClass({
  // this causes setState to run whenever the store calls this.trigger
  mixins: [Reflux.connect(SurveyStore), PureRenderMixin],
  componentDidMount() {
    SurveyActions.load();
  },
  handleAlertDismiss() {
  },
  handleLoadSurvey() {
    SurveyActions.loadSurvey();
  },
  handleDeleteSurvey() {
  },
  render() {
    const { modalState,
      numbers,
      pools,
      campaigns,
      alertState,
      surveyData,
      loadSurveyModalState,
      savedSurveys,
      optionGroupState } = this.state;

    const info = modalState.get('info');

    let modaleComponent, componentProps, title, type, id;
    componentProps = info;
    if (info) {
      title = info.title;
      id = info.id;
      type = info.type;
      if (info.type == 'QUESTION') {
        modaleComponent = Question;
        if (!componentProps) {
          componentProps = {};
        }
        if (!componentProps.multiple_choice_options) {
          componentProps.multiple_choice_options = {};
        }
        componentProps.multiple_choice_options.populateSelect = SurveyActions.populateSelect;
      } else if (info.type == 'GATHER_DIGITS') {

      }
      else if (info.type == 'FORWARD_TO_NUMBER') {
        componentProps.options = numbers;
        componentProps.number = componentProps.options[0].value;
        modaleComponent = ForwardToNumber;
      }
      else if (info.type == 'FORWARD_TO_CAMPAIGN') {
        componentProps.options = campaigns;
        componentProps.type = componentProps.options[0].value;
        modaleComponent = ForwardToCampaign;
      }
      else if (info.type == 'FORWARD_TO_POOL') {
        componentProps.options = pools;
        componentProps.type = componentProps.options[0].value;
        modaleComponent = ForwardToPool;
      }
      else if (info.type == 'PLAY_AUDIO') {
        modaleComponent = PlayAudio;
      }
      else if (info.type == 'SEND_SMS') {
        modaleComponent = SendSms;
      }
      else if (info.type == 'DECISION') {
        modaleComponent = Decision;
      }
      else if (info.type == 'KILL') {
        modaleComponent = Kill;
      }
    }
    return (
      <div className="row">
        <Modal
          id={id}
          type={type}
          title={title}
          toggleModal={SurveyActions.toggleModal}
          componentProps={componentProps}
          modaleComponent={modaleComponent}
          isOpen={modalState.get('isOpen')}
          parentID={modalState.get('dropTargetID')}
          questionDropped={SurveyActions.questionDropped}
          questionUpdated={SurveyActions.questionUpdated}
          />
        <LoadSurveyModal
          isOpen={loadSurveyModalState}
          savedSurveys={savedSurveys}
          />
        <AlertBox
          msg={alertState.get('msg')}
          level={alertState.get('level')}
          visible={alertState.get('visible')}
          />

        <div className="col-md-8">
          <Pallet survey={surveyData}/>
        </div>
        <div className="col-md-4">
          <div id="sidebar" data-spy="affix" data-offset-top="100" data-offset-bottom="50">
            <Toolbox
              optionGroups={optionGroupState.get('options')}
              optionGroupId={optionGroupState.get('selectedID')}/>
            <TreeView survey={surveyData}/>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = DragDropContext(HTML5Backend)(Application);
