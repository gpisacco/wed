const React = require('react');
const { List } = require('immutable');
const DraggableBlock = require('./DraggableBlock');
const DraggableOptionGroup = require('./DraggableOptionGroup');
const DraggableQuestion = require('./DraggableQuestion');
const OptionGroupArea = require('../../components/OptionGroupArea');

const Toolbox = React.createClass({
  propTypes: {
    optionGroupId: React.PropTypes.number.isRequired,
    optionGroups: React.PropTypes.instanceOf(List).isRequired
  },
  render(){
    const { optionGroupId, optionGroups } = this.props;
    return (
      <div className="toolbox">
        <h3>Toolbox <span className="help-text">Drop items on the pallet</span></h3>

        <div className="widgets-area">
          <DraggableBlock />
          <DraggableQuestion title="Question"
                             type="QUESTION"
                             color="#008148"/>
          <DraggableQuestion title="Play Audio"
                             type="PLAY_AUDIO"
                             color="#008118"/>
          <DraggableQuestion title="Decision"
                             type="DECISION"
                             color="#008118"/>
          <DraggableQuestion title="Gather Digits"
                             type="GATHER_DIGITS"
                             color="#008118"/>
          <DraggableQuestion title="Forward to Campaign"
                             type="FORWARD_TO_CAMPAIGN"
                             color="#008118"/>
          <DraggableQuestion title="Forward to Number"
                             type="FORWARD_TO_NUMBER"
                             color="#008118"/>
          <DraggableQuestion title="Forward to Pool"
                             type="FORWARD_TO_POOL"
                             color="#008118"/>
          <DraggableQuestion title="Send SMS"
                             type="SEND_SMS"
                             color="#008118"/>
          <DraggableQuestion title="Filter"
                             type="FILTER"
                             color="#008118"/>
          <DraggableQuestion title="Kill Call"
                             type="KILL"
                             color="#008118"/>
          <DraggableOptionGroup />
          <OptionGroupArea
            optionGroupId={optionGroupId}
            optionGroups={optionGroups}/>
        </div>
      </div>
    );
  }
});

module.exports = Toolbox;
