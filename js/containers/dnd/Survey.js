const React = require('react');
const SurveyActions = require('../../actions/SurveyActions');
const { List } = require('immutable');
const PureRenderMixin = require('react/addons').addons.PureRenderMixin;
const cx = require('classnames');
const { DropTarget } = require('react-dnd');
const ReactCSSTransitionGroup = require('react/addons').addons.CSSTransitionGroup;

const ItemTypes = require('../../constants/ItemTypes');

const Block = require('./Block');

const HelpText = require('../../components/HelpText');

const surveyTarget = {
  drop(props, monitor, component) {
    const droppedOnChild = !monitor.isOver({shallow: true});
    if (!droppedOnChild) {
      component.handleBlockDrop();
    }
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOverCurrent: monitor.isOver({shallow: true})
  };
}

function renderSubblocks(block) {
  const subblocks = block.get('subblocks');
  if (subblocks.count() > 0) {
    return subblocks.map((subb, i) =>
        <Block key={subb.get('id')}
               id={subb.get('id')}
               isFirst={i === 0}
               subblocks={subb.get('subblocks')}
               randomize={subb.get('randomize')}
               questions={subb.get('questions')}>
          {renderSubblocks(subb)}
        </Block>
    );
  }
}

const Survey = React.createClass({
  mixins: [PureRenderMixin],
  propTypes: {
    survey: React.PropTypes.instanceOf(List),
    isOverCurrent: React.PropTypes.bool,
    connectDropTarget: React.PropTypes.func
  },
  handleBlockDrop() {
    SurveyActions.blockDropped();
  },
  render() {
    const { survey, isOverCurrent, connectDropTarget } = this.props;

    const classes = cx({
      'survey': true,
      'dragging': isOverCurrent,
      'hovering': isOverCurrent
    });

    const blocks = survey.map((block, i) => {
      return (
        <Block key={block.get('id')}
               id={block.get('id')}
               isFirst={i === 0}
               subblocks={block.get('subblocks')}
               randomize={block.get('randomize')}
               questions={block.get('questions')}>
          {renderSubblocks(block)}
        </Block>
      );
    });

    // wrapping the blocks in a react transition group
    const blockAnimationTag = (
      <ReactCSSTransitionGroup transitionName="itemTransition" transitionEnter={false}>
        { blocks }
      </ReactCSSTransitionGroup>
    );

    return connectDropTarget(
      <div className={classes}>
        { survey.count() > 0 ? blockAnimationTag : <HelpText itemType="Block"/> }
      </div>
    );
  }
});

module.exports = DropTarget(ItemTypes.BLOCK, surveyTarget, collect)(Survey);
