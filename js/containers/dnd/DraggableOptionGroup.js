const React = require('react');
const { DragSource } = require('react-dnd');

const ItemTypes = require('../../constants/ItemTypes');

const optionGroupSource = {
  beginDrag() {
    return {item: {}};
  }
};

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

const DraggableOptionGroup = React.createClass({
  propTypes: {
    isDragging: React.PropTypes.bool.isRequired,
    connectDragSource: React.PropTypes.func.isRequired
  },
  render() {
    const { isDragging, connectDragSource } = this.props;

    return connectDragSource(
      <div style={{opacity: isDragging ? 0.4 : 1, backgroundColor: '#9d9c4f'}} className="draggable">
        <i className="ion-arrow-move"></i>
        Option List
      </div>
    );
  }
});

module.exports = DragSource(ItemTypes.OPTIONGROUP, optionGroupSource, collect)(DraggableOptionGroup);
