const React = require('react');
const { DragSource } = require('react-dnd');

const ItemTypes = require('../../constants/ItemTypes');

// this is the specification that describes
// how the drag source reacts to the drag and drop events
const blockSource = {
  beginDrag() {
    return {
      item: {}
    };
  }
};

// the collecting function that injects relevant props to the component
function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

const DraggableBlock = React.createClass({
  propTypes: {
    color: React.PropTypes.string.isRequired,
    isDragging: React.PropTypes.bool.isRequired,
    connectDragSource: React.PropTypes.func.isRequired,
    title: React.PropTypes.string.isRequired
  },
  render() {
    const { isDragging, connectDragSource } = this.props;

    return connectDragSource(
      <div style={{opacity: isDragging ? 0.4 : 1 }} className="draggable">
        <i className="ion-arrow-move"></i>
        Block
      </div>
    );
  }
});

module.exports = DragSource(ItemTypes.BLOCK, blockSource, collect)(DraggableBlock);
