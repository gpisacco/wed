const React = require('react');
const SurveyActions = require('../../actions/SurveyActions');
const flow = require('lodash/function/flow');
const { List } = require('immutable');
const PureRenderMixin = require('react/addons').addons.PureRenderMixin;
const cx = require('classnames');
const { DropTarget } = require('react-dnd');
const ReactCSSTransitionGroup = require('react/addons').addons.CSSTransitionGroup;

const ItemTypes = require('../../constants/ItemTypes');

const Question = require('./Question');
const ItemControl = require('../../components/ItemControl');
const HelpText = require('../../components/HelpText');
const ToggleParam = require('../../components/ToggleParam');

const blockTarget = {
  drop(props, monitor) {
    const droppedOnChild = !monitor.isOver({shallow: false});
    if (!droppedOnChild) {
      SurveyActions.blockDropped(props.id);
    }
  }
};

function blockCollect(connect, monitor) {
  return {
    connectBlockDropTarget: connect.dropTarget(),
    isBlockOver: monitor.isOver({shallow: true})
  };
}

const questionTarget = {
  drop(props, monitor, component) {
    const droppedOnChild = !monitor.isOver({shallow: false});
    if (!droppedOnChild) {
      SurveyActions.toggleModal(ItemTypes.QUESTION, props.id, monitor.getItem());
    }
  }
};

function questionCollect(connect, monitor) {
  return {
    connectQuestionDropTarget: connect.dropTarget(),
    isQuestionOver: monitor.isOver(),
    isOverChild: monitor.isOver({shallow: true})
  };
}

const Block = React.createClass({
  mixins: [PureRenderMixin],
  propTypes: {
    id: React.PropTypes.string.isRequired,
    isQuestionOver: React.PropTypes.bool.isRequired,
    isBlockOver: React.PropTypes.bool.isRequired,
    isOverChild: React.PropTypes.bool.isRequired,
    connectQuestionDropTarget: React.PropTypes.func.isRequired,
    connectBlockDropTarget: React.PropTypes.func.isRequired,
    children: React.PropTypes.object.isRequired,
    questions: React.PropTypes.instanceOf(List).isRequired,
    subblocks: React.PropTypes.instanceOf(List).isRequired,
    randomize: React.PropTypes.bool.isRequired,
    isFirst: React.PropTypes.bool.isRequired
  },
  handleQuestionDrop() {
    SurveyActions.toggleModal(ItemTypes.QUESTION, this.props.id);
  },
  handleDelete() {
    const deleteConfirmation = confirm("Are you sure you want to delete this block, its associated questions and options?");
    if (deleteConfirmation) {
      SurveyActions.itemDelete(ItemTypes.BLOCK, this.props.id);
    }
  },
  handleCopy() {
    SurveyActions.itemCopy(ItemTypes.BLOCK, this.props.id);
  },
  render() {
    const { isQuestionOver,
      isBlockOver,
      isOverChild,
      connectQuestionDropTarget,
      connectBlockDropTarget,
      isFirst,
      subblocks } = this.props;

    const classes = cx({
      'item block': true,
      'hovering': (isQuestionOver && !isOverChild) || isBlockOver,
      'first': isFirst
    });


    // render questions
    const questions = this.props.questions.map(q => {
        const questionType = q.get('questionType');
        const questionData = q.get('questionData');

        return <Question key={questionData.get('id')}
                         id={questionData.get('id')}
                         label={q.get('questionType') + ': ' + questionData.get('_label')}
                         questionData={questionData}
                         options={questionData.get('options')}
                         qtype={questionData.get('qtype')}
                         ordering={questionData.get('ordering')}
                         exclusive={questionData.get('exclusive')}
                         freetext={questionData.get('freetext')}/>

      }
    );

    // wrapping the questions in a react transition group
    const questionAnimationTag = (
      <ReactCSSTransitionGroup transitionName="itemTransition" transitionEnter={false}>
        { questions }
      </ReactCSSTransitionGroup>
    );
    const help = subblocks.count() > 0 ? null : <HelpText itemType="Question"/>;

    return connectBlockDropTarget(connectQuestionDropTarget(
      <div className={classes} id={this.props.id}>
        <div className="controls-area">
          <ul>
            <li><ItemControl icon="ion-trash-b" helpText="Delete this block" handleClick={this.handleDelete}/></li>
            <li><ItemControl icon="ion-plus-circled" helpText="Add a question" handleClick={this.handleQuestionDrop}/>
            </li>
            <li><ItemControl icon="ion-ios-copy" helpText="Clone this block" handleClick={this.handleCopy}/></li>
          </ul>
        </div>

        { questions.count() > 0 ? questionAnimationTag : help }

        { this.props.children }

        <div className="config-area">
          <ul>
            <li>
              <ToggleParam
                icon="ion-shuffle"
                helpText="Toggles whether questions will be randomized"
                toggleValue={this.props.randomize}
                toggleName='randomize'
                itemType={ItemTypes.BLOCK}
                itemId={this.props.id}/>
            </li>
          </ul>
        </div>
      </div>
    ));
  }
});

module.exports = flow(
  DropTarget(ItemTypes.QUESTION, questionTarget, questionCollect),
  DropTarget(ItemTypes.BLOCK, blockTarget, blockCollect)
)(Block);
