const React = require('react');
const { List } = require('immutable');
const SurveyActions = require('../../actions/SurveyActions');
const SurveyStore = require('../../stores/SurveyStore');
const PureRenderMixin = require('react/addons').addons.PureRenderMixin;

const ItemTypes = require('../../constants/ItemTypes');

const QuestionNode = require('./QuestionNode');
const BlockNode = require('./BlockNode');

const TreeView = React.createClass({
  mixins: [PureRenderMixin],
  propTypes: {
    survey: React.PropTypes.instanceOf(List).isRequired
  },
  getInitialState() {
    return {
      survey: this.props.survey,
      finalIndex: -1
    };
  },
  componentWillReceiveProps(nextProps) {
    this.setState({
      survey: nextProps.survey,
      finalIndex: -1
    });
  },
  renderProp(label, prop) {
    return prop ? <span><i className="ion-checkmark"></i>{label}</span> :
      <span><i className="ion-close"></i>{label}</span>;
  },
  ellipsize(text) {
    return text.substr(0, 20) + (text.length > 20 ? "..." : "");
  },
  focusOnItem(id) {
    SurveyActions.scrollToItem(id);
  },
  handleDrop(sourceID, targetID) {
    const sourceType = sourceID[0] === "q" ? ItemTypes.QUESTION : ItemTypes.BLOCK;
    const targetType = targetID[0] === "b" ? ItemTypes.BLOCK : ItemTypes.QUESTION;

    if (sourceType === ItemTypes.QUESTION && targetType === ItemTypes.BLOCK) {
      // when a question is dropped in a block
      SurveyActions.moveQuestion(sourceID, targetID);
    } else {
      // when a question is dropped on a question or a block is dropped on a block
      SurveyActions.reorderItem(sourceID, this.state.finalIndex, sourceType);
    }
  },
  reorderBlock(draggedBlockId, overBlockId) {
    const survey = this.state.survey;
    const draggedBlockIndex = survey.findIndex(b => b.get('id') === draggedBlockId);
    const overBlockIndex = survey.findIndex(b => b.get('id') === overBlockId);

    const block = survey.get(draggedBlockIndex);
    const newSurvey = survey.delete(draggedBlockIndex).splice(overBlockIndex, 0, block);
    this.setState({
      survey: newSurvey,
      finalIndex: overBlockIndex
    });
  },
  reorderQuestion(draggedQuestionId, overQuestionId) {
    const survey = this.state.survey;
    let newSurvey;

    // get indices of the blocks holding the drag and drop targets (questions)
    const draggedBlockId = SurveyStore.getBlockId(draggedQuestionId);
    const overBlockId = SurveyStore.getBlockId(overQuestionId);
    const draggedBlockIndex = survey.findIndex(b => b.get('id') === draggedBlockId);
    const overBlockIndex = survey.findIndex(b => b.get('id') === overBlockId);
    const draggedBlock = survey.get(draggedBlockIndex);
    const overBlock = survey.get(overBlockIndex);
    
    // get indices of the drop and drag targets (questions) themselves
    const draggedQuestionIndex = draggedBlock.get('questions')
      .findIndex(q => q.get('id') === draggedQuestionId);

    // this gives an error - q is undefined
    const overQuestionIndex = overBlock.get('questions')
      .findIndex(q => q.get('id') === overQuestionId);
    
    // cache the question being dragged
    const draggedQuestion = draggedBlock.getIn(['questions', draggedQuestionIndex]);

    // ensure that the question is being ordered withtin the same block
    if (draggedBlockId === overBlockId) {
      newSurvey = survey.updateIn([draggedBlockIndex, 'questions'],
        (questions) => questions.delete(draggedQuestionIndex)
          .splice(overQuestionIndex, 0, draggedQuestion)
      );

      // change the state
      this.setState({
        survey: newSurvey,
        finalIndex: overQuestionIndex
      });
    }
  },
  renderSubblocks(block) {
    const subblocks = block.get('subblocks');
    const self = this;
    return subblocks.map(subb =>
        <BlockNode key={subb.get('id')} id={subb.get('id')}
                   handleClick={self.focusOnItem.bind(this, subb.get('id'))}
                   handleDrop={self.handleDrop}
                   reorderBlock={self.reorderBlock}>

          {self.renderSubblocks(subb)}

          {subb.get('questions').map(q =>
              <QuestionNode id={q.get('id')}
                            key={q.get('id')}
                            label={self.ellipsize(q.get('questionType') + ': ' + questionData.get('_label'))}
                            handleClick={self.focusOnItem.bind(this, q.get('id'))}
                            handleDrop={self.handleDrop}
                            reorderQuestion={self.reorderQuestion}>
                <div className="tree-view_node">{"Options: " + q.get('options').count()}</div>
                <div className="tree-view_node">{self.renderProp('ordering', q.get('ordering'))}</div>
                <div className="tree-view_node">{self.renderProp('exclusive', q.get('exclusive'))}</div>
                <div className="tree-view_node">{self.renderProp('freetext', q.get('freetext'))}</div>
              </QuestionNode>
          )}
        </BlockNode>
    );
  },
  render() {
    const { survey } = this.state;
    const self = this;

    // build the tree
    const tree = survey.map((block) => {
      const questions = block.get('questions');
      return (
        <BlockNode key={block.get('id')} id={block.get('id')}
                   handleClick={self.focusOnItem.bind(this, block.get('id'))}
                   handleDrop={self.handleDrop}
                   reorderBlock={self.reorderBlock}>

          {self.renderSubblocks(block)}

          {questions.map(q => {
              const questionType = q.get('questionType');
              const questionData = q.get('questionData');

              return <QuestionNode id={questionData.get('id')}
                                   key={questionData.get('id')}
                                   label={self.ellipsize(q.get('questionType') + ': ' + questionData.get('_label'))}
                                   handleClick={self.focusOnItem.bind(this, questionData.get('id'))}
                                   handleDrop={self.handleDrop}
                                   reorderQuestion={self.reorderQuestion}>
                
              </QuestionNode>
            }
          )}

        </BlockNode>
      );
    });

    return (
      <div className="tree-view">
        <h3>Minimap <span className="help-text">Drag items to re-order</span></h3>
        {tree}
      </div>
    );
  }
});

module.exports = TreeView;
