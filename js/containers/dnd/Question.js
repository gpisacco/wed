const React = require('react');
const SurveyActions = require('../../actions/SurveyActions');
const { List } = require('immutable');
const PureRenderMixin = require('react/addons').addons.PureRenderMixin;
const cx = require('classnames');
const { DropTarget } = require('react-dnd');

const ItemTypes = require('../../constants/ItemTypes');
const Options = require('../../components/Options');
const ItemControl = require('../../components/ItemControl');
const ToggleParam = require('../../components/ToggleParam');

const questionTarget = {
  drop(props, monitor, component) {
    component.handleOptionGroupDrop();
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    canDrop: monitor.canDrop(),
    isOver: monitor.isOver()
  };
}

const Question = React.createClass({
  mixins: [PureRenderMixin],
  propTypes: {
    options: React.PropTypes.instanceOf(List).isRequired,
    id: React.PropTypes.string.isRequired,
    qtext: React.PropTypes.string.isRequired,
    ordering: React.PropTypes.bool.isRequired,
    freetext: React.PropTypes.oneOfType([
      React.PropTypes.bool.isRequired,
      React.PropTypes.string.isRequired
    ]),
    exclusive: React.PropTypes.bool.isRequired,
    connectDropTarget: React.PropTypes.func.isRequired,
    isOver: React.PropTypes.bool.isRequired,
    canDrop: React.PropTypes.bool.isRequired
  },
  handleOptionGroupDrop() {
    SurveyActions.optionGroupDropped(this.props.id);
  },
  getInitialState: function () {
    return {
      editing: false,
      changed: false
    };
  },
  handleDelete() {
    const deleteConfirmation = confirm("Are you sure you want to delete this question and its options?");
    if (deleteConfirmation) {
      SurveyActions.itemDelete(ItemTypes.QUESTION, this.props.id);
    }
  },
  // toggles the state of question text field to editing
  toggleInput() {
    this.setState({
      editing: true
    });
  },
  /**
   * handler called when the textinput loses focus or detects change.
   * responsible for calling relevant action that saves the new text.
   */
    handleEdit(e) {
    if (e.type === "blur" ||
      (e.type === "keypress" && e.key === "Enter")) {

      this.setState({editing: false});

      // if the new value is blank do nothing
      if (e.target.value.trim() === "") {
        return;
      }
      SurveyActions.saveEditText(e.target.value, this.props.id);
    }
  },
  handleFreeTextEdit(e) {
    if (e.type === "keypress" && e.key === "Enter") {
      if (e.target.value.trim() === "") {
        return;
      }
      SurveyActions.saveFreeText(e.target.value, this.props.id);
    }
  },
  handleCopy() {
    SurveyActions.itemCopy(ItemTypes.QUESTION, this.props.id);
  },
  handleDoubleClick() {
    SurveyActions.toggleModal(ItemTypes.QUESTION, this.props.id, this.props.questionData.toJS());
  },
  render: function () {
    const { canDrop, isOver, connectDropTarget, freetext } = this.props;

    const classes = cx({
      'item question': true,
      'dragging': canDrop,
      'hovering': isOver
    });

    // placeholder for taking input when text is clicked
    const input = (<input type="text"
                          defaultValue={this.props.qtext}
                          style={{width: 200}}
                          onBlur={this.handleEdit}
                          onKeyPress={this.handleEdit}/>);

    const freeTextInput = (<input type="text"
                                  placeholder="Default text (if any). Press Enter to save."
                                  defaultValue={freetext ? null : freetext}
                                  onKeyPress={this.handleFreeTextEdit}/>);

    return connectDropTarget(
      <div className={classes} id={this.props.id} onDoubleClick={this.handleDoubleClick}>

        <div className="qtext-area">
                  <span className="qtext" onClick={this.toggleInput}>
                  { this.state.editing ? input : this.props.label  }
                  </span>

          <div className="controls-area">
            <ul>
              <li><ItemControl icon="ion-trash-b" helpText="Delete this question" handleClick={this.handleDelete}/></li>
              <li><ItemControl icon="ion-ios-copy" helpText="Clone this question" handleClick={this.handleCopy}/></li>
            </ul>
          </div>
        </div>

        {this.props.options ? <Options options={this.props.options} questionId={this.props.id}/>:null}

        <div className="config-area">
          <ul>
            {this.props.ordering ? <li>
              <ToggleParam
                icon="ion-shuffle"
                helpText="Toggles whether options are randomized"
                toggleValue={this.props.ordering}
                toggleName='ordering'
                itemType={ItemTypes.QUESTION}
                itemId={this.props.id}/>
            </li> : null}
            {this.props.exclusive ? <li>
              <ToggleParam
                icon="ion-android-radio-button-on"
                helpText="Toggles whether options appear as radio button or checkbox"
                toggleValue={this.props.exclusive}
                toggleName='exclusive'
                itemType={ItemTypes.QUESTION}
                itemId={this.props.id}/>
            </li> : null}
            {this.props.freetext ? <li>
              <ToggleParam
                icon="ion-document-text"
                helpText="Toggles whether free text can be entered"
                toggleValue={typeof this.props.freetext === "string"}
                toggleName='freetext'
                itemType={ItemTypes.QUESTION}
                itemId={this.props.id}/>
            </li> : null}
          </ul>
        </div>

        <div className="freeText-area">
          { this.props.freetext ? freeTextInput : null}
        </div>

      </div>
    );
  }
});

module.exports = DropTarget(ItemTypes.OPTIONGROUP, questionTarget, collect)(Question);
