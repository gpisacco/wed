const React = require('react');
const { DragSource } = require('react-dnd');

const ItemTypes = require('../../constants/ItemTypes');

const questionSource = {
  beginDrag(props) {
    return { type: props.type , title: props.title};
  }
};

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

const DraggableQuestion = React.createClass({
  propTypes: {
    color: React.PropTypes.string.isRequired,
    isDragging: React.PropTypes.bool.isRequired,
    connectDragSource: React.PropTypes.func.isRequired,
    title: React.PropTypes.string.isRequired
  },
  render() {
    const blockColor = this.props.color;
    const { isDragging, connectDragSource } = this.props;
    return connectDragSource(
      <div style={{opacity: isDragging ? 0.4 : 1,
                        backgroundColor: blockColor}} className="draggable">
        <i className="ion-arrow-move"></i>
        {this.props.title}
      </div>
    );
  }
});

module.exports = DragSource(ItemTypes.QUESTION, questionSource, collect)(DraggableQuestion);
