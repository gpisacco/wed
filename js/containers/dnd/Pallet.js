const React = require('react');
const { List } = require('immutable');
const PureRenderMixin = require('react/addons').addons.PureRenderMixin;

const Survey = require('./Survey');
const Controls = require('../../components/Controls');

const Pallet = React.createClass({
  mixins: [PureRenderMixin],
  propTypes: {
    survey: React.PropTypes.instanceOf(List)
  },
  render: function () {
    return (
      <div>
        <Controls />

        <div className="survey-area">
          <Survey survey={this.props.survey}/>
        </div>
      </div>
    );
  }
});

module.exports = Pallet;
