const React = require('react');
const cx = require('classnames');
const { DragSource, DropTarget } = require('react-dnd');
const flow = require('lodash/function/flow');
const PureRenderMixin = require('react/addons').addons.PureRenderMixin;

const ItemTypes = require('../../constants/ItemTypes');

/* setup for dragging block node */
const nodeSource = {
  beginDrag(props) {
    return {id: props.id};
  }
};

function dragCollect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

/* setup for allowing blocks to act as drop targets
 * for questions */
const questionTarget = {
  drop(props, monitor, component) {
    const droppedOnChild = !monitor.isOver({shallow: false});
    if (!droppedOnChild) {
      props.handleDrop(monitor.getItem().id, props.id);
    }
  }
};

function dropCollect(connect) {
  return {
    connectDropTarget: connect.dropTarget()
  };
}

/* setup for allowing blocks to act as drop target for other blocks.
 * this is required to implement sortable on blocks */
const blockTarget = {
  hover(props, monitor) {
    const draggedId = monitor.getItem().id;
    if (draggedId !== props.id) {
      props.reorderBlock(draggedId, props.id);
    }
  },
  drop(props) {
    // called when the hover ends - used to propagate
    // changes upstream
    props.handleDrop(props.id, props.id);
  }
};

function sortCollect(connect) {
  return {
    connectSortTarget: connect.dropTarget()
  };
}

const BlockNode = React.createClass({
  mixins: [PureRenderMixin],
  propTypes: {
    collapsed: React.PropTypes.bool,
    isDragging: React.PropTypes.bool,
    connectDragSource: React.PropTypes.func,
    connectDropTarget: React.PropTypes.func,
    connectSortTarget: React.PropTypes.func,
    children: React.PropTypes.object,
    defaultCollapsed: React.PropTypes.bool,
    id: React.PropTypes.string.isRequired,
    handleClick: React.PropTypes.func.isRequired,
    reorderBlock: React.PropTypes.func.isRequired,
    moveBlock: React.PropTypes.func
  },
  getInitialState() {
    return {
      collapsed: this.props.defaultCollapsed || true
    };
  },
  handleCollapse() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  },
  render() {
    const collapsed = this.props.collapsed !== null ?
      this.props.collapsed : this.state.collapsed;

    const { isDragging,
      connectDragSource,
      id,
      connectDropTarget,
      connectSortTarget,
      handleClick,
      children } = this.props;

    const arrowClass = cx({
      'ion-arrow-down-b': !collapsed,
      'ion-arrow-right-b': collapsed
    });

    const arrow = (<div onClick={this.handleCollapse} className="tree-view_arrow">
      <i className={arrowClass}></i>
    </div>);

    return connectSortTarget(connectDragSource(connectDropTarget(
      <div className='tree-view_node-block' style={{opacity: isDragging ? 0 : 1}}> {arrow}
        <span className="tree-view_block-title" onClick={handleClick}>{"Block #" + id}</span>
        { collapsed ? null : <div className="tree-view_children">{children}</div> }
      </div>
    )));
  }
});

module.exports = flow(
  DragSource(ItemTypes.BLOCKNODE, nodeSource, dragCollect),
  DropTarget(ItemTypes.BLOCKNODE, blockTarget, sortCollect),
  DropTarget(ItemTypes.QUESTIONNODE, questionTarget, dropCollect)
)(BlockNode);
