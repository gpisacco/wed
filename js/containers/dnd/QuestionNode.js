const React = require('react');
const { DragSource, DropTarget } = require('react-dnd');
const cx = require('classnames');
const flow = require('lodash/function/flow');
const PureRenderMixin = require('react/addons').addons.PureRenderMixin;

const ItemTypes = require('../../constants/ItemTypes');

/* setup for dragging question nodes */
const nodeSource = {
  beginDrag(props) {
    return {id: props.id};
  }
};

function dragCollect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

/* setup for allowing questions to act as drop targets for questions.
 * this is required to implement sortable on questions */
const questionTarget = {
  hover(props, monitor) {
    const draggedId = monitor.getItem().id;
    if (draggedId !== props.id) {
      props.reorderQuestion(draggedId, props.id);
    }
  },
  // called when the hover ends - used to propagate changes upstream
  drop(props) {
    props.handleDrop(props.id, props.id);
  }
};

function dropCollect(connect) {
  return {
    connectDropTarget: connect.dropTarget()
  };
}

const QuestionNode = React.createClass({
  mixins: [PureRenderMixin],
  propTypes: {
    collapsed: React.PropTypes.bool,
    defaultCollapsed: React.PropTypes.bool,
    label: React.PropTypes.node.isRequired,
    id: React.PropTypes.string.isRequired,
    handleDrop: React.PropTypes.func.isRequired,
    handleClick: React.PropTypes.func.isRequired,
    reorderQuestion: React.PropTypes.func.isRequired,
    connectDragSource: React.PropTypes.func.isRequired,
    connectDropTarget: React.PropTypes.func.isRequired,
    isDragging: React.PropTypes.bool.isRequired,
    children: React.PropTypes.object.isRequired
  },
  getInitialState() {
    return {
      collapsed: this.props.defaultCollapsed || true
    };
  },
  handleCollapse() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  },
  render() {
    const props = this.props;

    const collapsed = props.collapsed !== null ?
      props.collapsed : this.state.collapsed;

    const { isDragging,
      connectDragSource,
      connectDropTarget } = this.props;

    const arrowClass = cx({
      'ion-arrow-down-b': !collapsed,
      'ion-arrow-right-b': collapsed
    });

    const arrow = (<div onClick={this.handleCollapse} className="tree-view_arrow">
      <i className={arrowClass}></i>
    </div>);

    return connectDropTarget(connectDragSource(
      <div className='tree-view_node-question' style={{opacity: isDragging ? 0 : 1}}> {arrow}
        <span onClick={props.handleClick} className="tree-view_question-title">{this.props.label}</span>
        { collapsed ? null : <div className="tree-view_children">{this.props.children}</div> }
      </div>
    ));
  }
});

module.exports = flow(
  DragSource(ItemTypes.QUESTIONNODE, nodeSource, dragCollect),
  DropTarget(ItemTypes.QUESTIONNODE, questionTarget, dropCollect)
)(QuestionNode);
