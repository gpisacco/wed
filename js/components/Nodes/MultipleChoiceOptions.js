const React = require('react');
const R = require('ramda');
const PureRenderMixin = require('react/addons').addons.PureRenderMixin;
const cx = require('classnames');
const Select = require('react-select');
const ItemTypes = require('../../constants/ItemTypes');
const MultipleChoiceOption = require('./MultipleChoiceOption');
const SurveyActions = require('../../actions/SurveyActions');
const SurveyStore = require('../../stores/SurveyStore');

const MultipleChoiceOptions = React.createClass({
  mixins: [PureRenderMixin],
  getInitialState: function () {
    const selectOptions = [
      {
        value: "invalid",
        label: "Invalid",
        selected: true
      },
      {
        value: "terminate_call",
        label: "Terminate Call"
      },
      {
        value: "next_question",
        label: "Go To Next Question"
      }
    ];
    
    
    return {
      editing: false,
      userOptions: {},
      selectOptions
    };
  },
  componentDidMount: function() {
      return this.props.populateSelect(this);
   },
  propTypes: {
    display: React.PropTypes.string,
    id: React.PropTypes.number,
    options: React.PropTypes.array,
  },
  submit(){
    const promise = Promise.all(R.map(([key, val])=>(
      val.submit()
        .then((data)=>([key,data]))
    ))(R.toPairs(this.refs)))
      .then(R.fromPairs);

    return promise
      .then((multiple_choice_options)=> {
        this.setState({multiple_choice_options})
      })
      .then(()=> {
        if (this.props.handleSubmit) {
          this.props.handleSubmit(this.state);
        }
        return this.state;
      }
    )

  },
  render: function () {
    const disp = this.props.display === undefined ? 'none' : this.props.display;
    if (disp == 'multiple_choice') {
      const ret = [];
      for (let i = 0; i < 9; i++) {
          if(this.state.multiple_choice_options !== undefined )
          {
              this.props.options[i] = this.state.multiple_choice_options['qMultipleChoice-d'+i];
              this.props.options[i].number = i;
              this.props.options[i].id = 'qMultipleChoice-d'+i;
          }
        if (this.props.options[i] === undefined) {
            if(this.state.multiple_choice_options !== undefined )
            {
                this.props.options[i] = this.state.multiple_choice_options[i];
            }else {
              this.props.options[i] = {
                number : i+1,
                label : "",
                transition:undefined
              };
          }
        }
        ret.push(
          <div key={"qMultipleChoice-d" + i} className="col-lg-12 mc-item">
            <div className="col-lg-2 number">
              Press {i}:
            </div>
            <div className="col-lg-10 small">
              <MultipleChoiceOption
              ref={"qMultipleChoice-d" + i}
              options={this.state.selectOptions} 
              action={this.props.options[i].action}
              label={this.props.options[i].label} 
              transition ={this.props.options[i].transition} 
              number={i + 1}
                />
            </div>
          </div>
        );
      }

      return (<div className="form-group multiple-choice" style={{display: disp }}>
        <label htmlFor="qrating">What multiple choice options can the person enter?</label>
        {ret}
        {this.props.isDebug ? <button onClick={this.submit}>Submit</button> : null}
      </div>

      );
    }
    return (<div></div>);
  }
});

module.exports = MultipleChoiceOptions;
