const React = require('react');
const PlayAudio = require('./PlayAudio');

export default React.createClass({
  getInitialState() {
    return {
      text: this.props.text || "",
      number: this.props.number || ""
    };
  },
  propTypes: {
    text: React.PropTypes.string,
    audio: React.PropTypes.object,
    number: React.PropTypes.string,
    handleSubmit: React.PropTypes.func
  },
  submit(){
    return this.refs.audio.submit()
      .then((audio)=> this.setState({audio}))
      .then(()=> {
        if (this.props.handleSubmit) {
          this.props.handleSubmit(this.state);
        }
        return this.state;
      }
    ).then(()=> this.setState({_label:this.state.text}))
     .then(()=> this.state);
  },
  render() {
    return (
      <div>
        <div className="form-group">
          <label >From Number</label>
          <input type="text"
                 className="form-control"
                 ref="number"
                 value={this.state.number}
                 onChange={(event)=>this.setState({number: event.target.value})}
            />
        </div>
        <PlayAudio ref='audio' {...this.props.audio}/>

        <div className="form-group">
          <label>Text</label>
          <input type="text"
                 maxLength="160"
                 placeholder="Thanks for your call"
                 className="form-control"
                 ref="text"
                 value={this.state.text}
                 onChange={(event)=>this.setState({text: event.target.value})}/>

        </div>
        {this.props.isDebug ? <button onClick={this.submit}>Submit</button> : null}
      </div>
    );
  }
});
