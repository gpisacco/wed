const React = require('react');
const Select = require('react-select');

export default React.createClass({
  getInitialState() {
    return {
      type: this.props.type || "block_call"
    };
  },
  propTypes: {
    type: React.PropTypes.string,
    handleSubmit: React.PropTypes.func
  },
  submit(){
    if (this.props.handleSubmit) {
      this.props.handleSubmit(this.state)
    }
    return Promise.resolve(this.state)
      .then(()=> this.setState({_label:this.state.type}))
      .then(()=> this.state);
  },
  render() {
    this.options = {};
    return (
      <div>
        <Select onChange={(type)=>this.setState({type})}
                options={
                   [{
                      label: "Block Call",
                      value: "block_call"
                    },
                    {
                      label: "Invalid Caller",
                      value: "invalid_caller"
                    }]}
                value={this.state.type}/>
        {this.props.isDebug ? <button onClick={this.submit}>Submit</button> : null}
      </div>
    );
  }
});


