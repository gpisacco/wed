const React = require('react');
const { List } = require('immutable');
const Select = require('react-select');

const MultipleChoiceOption = React.createClass({
  getInitialState() {
    return {
      transition: this.props.transition,
      label: this.props.label,
      options : this.props.options
    };
  },
  submit(){
    if (this.props.handleSubmit) {
      this.props.handleSubmit(this.state)
    }
    return Promise.resolve(this.state)
  },
  propTypes: {
    number: React.PropTypes.number,
    transition: React.PropTypes.string,
    label: React.PropTypes.string,
    options: React.PropTypes.array
  },
  render() {
    const { options, number, action} = this.props;
    return (
      <div className="form-group">
        <div className="col-lg-12">
          <div className="col-lg-6">
            <input type="text"
                   placeholder="Option Name?"
                   value={this.state.label}
                   className="form-control"
                   key={"qMultipleChoiceValue-" + number}
                   id={"qMultipleChoiceValue-" + number}
                   ref={"qMultipleChoiceValue-" + number}
                   onChange={(event)=>{this.setState({label: event.target.value})}}
              />
          </div>
          <div className="col-lg-6">
            <Select onChange={(val)=> {this.setState({transition: val})}}
                    key={"qMultipleChoice-" + number}
                    id={"qMultipleChoice-" + number}
                    ref={"qMultipleChoice-" + number}
                    value={this.state.transition == "" ? this.state.options[0] : this.state.transition}
                    options={this.state.options}/>
          </div>
        </div>
        {this.props.isDebug ? <button onClick={this.submit}>Submit</button> : null}
      </div>)
  }
});

module.exports = MultipleChoiceOption;
