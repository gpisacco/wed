const React = require('react');
const Select = require('react-select');
const Button = require('react-bootstrap').Button;
const Row = require('react-bootstrap').Row;
const Col = require('react-bootstrap').Col;
const Grid = require('react-bootstrap').Grid;

export default React.createClass({
  getInitialState() {
    return {
      text: this.props.text || "",
      match_type: this.props.qtype || "match_canada_calls",
      go_next_type: this.props.qtype || "go_to_next_node"
    };
  },
  propTypes: {
    text: React.PropTypes.string,
    match_type: React.PropTypes.string,
    go_next_type: React.PropTypes.string,
    handleSubmit: React.PropTypes.func
  },
  submit(){
    if (this.props.handleSubmit) {
      this.props.handleSubmit(this.state)
    }
    return Promise.resolve(this.state)
      .then(()=> this.setState({_label:this.state.text}))
      .then(()=> this.state);
  },
  addToTextArea(val){
    if(this.state.text === ''){
      this.setState({text: val});
      return;
    }
    this.setState({text: this.state.text + ' ' + val})
  },
  render() {
    return (
      <div>
        <Select onChange={(match_type)=>this.setState({match_type})}
                options={
                   [{
                      label: "Match Canada Calls",
                      value: "match_canada_calls"
                    },
                    {
                      label: "Match Canada Non Nanpa Calls",
                      value: "match_canada_non_nanpa_calls"
                    },
                    {
                      label: "Match Weekend Calls",
                      value: "match_weekend_calls"
                    },
                    {
                      label: "Match Non Office Hours Calls",
                      value: "match_non_office_hours_calls"
                    },
                    {
                      label: "Match Mobile Calls",
                      value: "match_mobile_calls"
                    },
                    {
                      label: "Match Calls From Specific Area Codes",
                      value: "match_calls_from_specific_area_codes"
                    }
                    ]}
                value={this.state.match_type}/>


        <div className="form-group">
          <label>Variables</label>
        </div>
        <div className="form-group">
          <Button onClick={()=>this.addToTextArea('STATE')}>State</Button>
          <Button onClick={()=>this.addToTextArea('AREA_CODE')}>Area Code</Button>
          <Button onClick={()=>this.addToTextArea('COUNTRY')}>Country</Button>
          <Button onClick={()=>this.addToTextArea('COUNTRY_CODE')}>Country Code</Button>
          <Button onClick={()=>this.addToTextArea('HOUR')}>Hour</Button>
          <Button onClick={()=>this.addToTextArea('MINUTES')}>Minutes</Button>
          <Button onClick={()=>this.addToTextArea('DOW')}>Day of Week</Button>
          <Button onClick={()=>this.addToTextArea('DAY_OF_MONTH')}>Day of Month</Button>
          <Button onClick={()=>this.addToTextArea('YEAR')}>Year</Button>
          <Button onClick={()=>this.addToTextArea('POOL')}>Pool</Button>
          <Button onClick={()=>this.addToTextArea('CAMPAIGN')}>Campaign</Button>
          <Button onClick={()=>this.addToTextArea('SOURCE_GROUP')}>Source Group</Button>
          <Button onClick={()=>this.addToTextArea('FROM_NUMBER')}>From Number</Button>
          <Button onClick={()=>this.addToTextArea('TO_NUMBER')}>To Number</Button>
          <Button onClick={()=>this.addToTextArea('MEMBER_TYPE')}>Member Type</Button>
        </div>

        <div className="form-group">
          <label>Operators</label>
        </div>
        <div className="form-group">
          <Button onClick={()=>this.addToTextArea('==')} bsStyle='primary'>Equal</Button>
          <Button onClick={()=>this.addToTextArea('!=')} bsStyle='primary'>Not Equal</Button>
          <Button onClick={()=>this.addToTextArea('IN')} bsStyle='primary'>In</Button>
          <Button onClick={()=>this.addToTextArea('NOT_IN')} bsStyle='primary'>Not In</Button>
          <Button onClick={()=>this.addToTextArea('>')} bsStyle='primary'>Greater Than</Button>
          <Button onClick={()=>this.addToTextArea('<')} bsStyle='primary'>Less Than</Button>
          <Button onClick={()=>this.addToTextArea('and')} bsStyle='primary'>AND</Button>
          <Button onClick={()=>this.addToTextArea('or')} bsStyle='primary'>OR</Button>
        </div>

        <Row>
          <Col md={6}>
            <div className="form-group">
              <label>If</label>
              <textarea
                className="form-control"
                rows="5"
                value={this.state.text}
                placeholder="STATE in ['CA','FL']"
                ref="number"
                onChange={(event)=>this.setState({text: event.target.value})}
                />
            </div>
          </Col>
          <Col md={6}>
            <div className="form-group">
              <label></label>
            </div>
            <Select onChange={(go_next_type)=>this.setState({go_next_type})}
                    options={
                   [{
                      label: "Go to next node",
                      value: "go_to_next_node"
                    },
                    {
                      label: "Terminate call",
                      value: "terminate_call"
                    },
                    {
                      label: "Go to node xxxx",
                      value: "go_to_node_xxxx"
                    },
                    {
                      label: "Go to node yyyy",
                      value: "go_to_node_yyyy"
                    }
                    ]}
                    value={this.state.go_next_type}/>
          </Col>
        </Row>
        {this.props.isDebug ? <button onClick={this.submit}>Submit</button> : null}
      </div>
    );
  }
});
