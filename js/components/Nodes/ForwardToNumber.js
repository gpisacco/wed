const React = require('react');
const Select = require('react-select');

export default React.createClass({
  getInitialState() {
    return {
      text: this.props.text || "",
      number: this.props.number,
      record: this.props.record || false
    };
  },
  propTypes: {
    text: React.PropTypes.string,
    number: React.PropTypes.string,
    record: React.PropTypes.bool,
    handleSubmit: React.PropTypes.func
  },
  submit(){
    if (this.props.handleSubmit) {
      this.props.handleSubmit(this.state)
    }
    return Promise.resolve(this.state)
      .then(()=> this.setState({_label:this.state.number}))
      .then(()=> this.state);
  },
  render() {
    return (
      <div>
        <div className="form-group">
          <label >Number</label>
          <Select onChange={(number)=>this.setState({number})}
                  options={this.props.options}
                  value={this.state.number}/>
        </div>
        <div className="checkbox">
          <label>
            <input type="checkbox"
                   ref="record"
                   checked={this.state.record}
                   onChange={(event)=>this.setState({record: event.target.checked})}
              />
            Record</label>
        </div>
        <div className="form-group">
          <label >Max Duration (Seconds)</label>
          <input type="text"
                 placeholder="7200"
                 className="form-control"
                 value={this.state.text}
                 ref="number"
                 onChange={(event)=>this.setState({text: event.target.value})}
            />
        </div>
        {this.props.isDebug ? <button onClick={this.submit}>Submit</button> : null}
      </div>
    );
  }
});
