const React = require('react');
const PureRenderMixin = require('react/addons').addons.PureRenderMixin;
const ItemTypes = require('../../constants/ItemTypes');

const RatingOptions = React.createClass({
  mixins: [PureRenderMixin],
  getInitialState: function () {
    return {
      rating: this.props.rating || 0
    };
  },
  propTypes: {
    id: React.PropTypes.string.isRequired,
    display: React.PropTypes.string.isRequired,
    rating: React.PropTypes.number.isRequired
  },
  submit(){
    if (this.props.handleSubmit) {
      this.props.handleSubmit(this.state)
    }
    return Promise.resolve(this.state);
  },
  render: function () {
    const disp = this.props.display === undefined ? 'none' : this.props.display;
    if (disp !== 'rating') {
      return <div/>;
    }

    return (
      <div className="form-group"
           style={{display: disp }}>
        <label htmlFor="rating">Ask the respondent to pick a rating from 1 to: </label>
        <input type="text"
               placeholder="up to 9"
               className="form-control"
               id="rating"
               ref="rating"
               value={this.state.rating}
               onChange={(event)=>this.setState({rating: event.target.value})}
          />
        {this.props.isDebug ? <button onClick={this.submit}>Submit</button> : null}
      </div>
    );
  }
});

module.exports = RatingOptions;
