const $ = require('jquery');
const React = require('react');
const Modal = require('react-bootstrap').Modal;
const Select = require('react-select');
const ItemTypes = require('../../constants/ItemTypes');
const RatingOptions = require('./RatingOptions');
const PlayAudio = require('./PlayAudio');
const MultipleChoiceOptions = require('./MultipleChoiceOptions');

export default React.createClass({
  getInitialState() {
    return {
      qtext: this.props.qtext || "",
      options: this.props.options || [],
      qtype: this.props.qtype || "rating",
      showAudio: 'none',
      audio: null
    };
  },
  propTypes: {
    options: React.PropTypes.array,
    multiple_choice_options: React.PropTypes.object.isRequired,
    rating_options: React.PropTypes.object.isRequired,
    qtext: React.PropTypes.string,
    qtype: React.PropTypes.string,
    showAudio: React.PropTypes.string,
    audio: React.PropTypes.object,
    handleSubmit: React.PropTypes.func.isRequired
  },
  componentDidMount() {
    const textInput = this.refs.qtext.getDOMNode();
    textInput.focus();
  },
  submit(){
    if (this.state.qtext == "") {
      const textInput = this.refs.qtext.getDOMNode();
      textInput.focus();
    }
    return this.refs.rating_options.submit()
      .then((rating_options)=> this.setState({rating_options}))
      .then(this.refs.multiple_choice_options.submit())
      .then((multiple_choice_options)=> this.setState({multiple_choice_options}))
      .then(this.refs.audio.submit())
      .then((audio)=> this.setState({audio}))
      .then(()=> {
        if (this.props.handleSubmit) {
          this.props.handleSubmit(this.state);
        }
        return this.state;
        }
      )
      .then(()=> this.setState({_label:this.state.qtext}))
      .then(()=> this.state);
  },
  render() {
    return (<div>
      <div className="form-group">
        <label htmlFor="qtype">Question Type</label>
        <Select id="qtype"
                ref="qtype"
                onChange={(qtype)=>this.setState({qtype})}
                options={
                   [{
                      value: "multiple_choice",
                      label: "Multiple Choice"
                    },
                    {
                      value: "rating",
                      label: "Rating"
                    },
                    {
                      value: "enter_a_number",
                      label: "Enter a Number"
                    }]}
                value={this.state.qtype}/>
      </div>

      <div className="form-group">
        <label htmlFor="qtext">Question Text</label>
        <input type="text"
               placeholder="What is value of 4 + 5?"
               className="form-control"
               id="qtext"
               ref="qtext"
               value={this.state.qtext}
               onChange={(event) =>{
                                // submit on enter key press
                                if (event.key === "Enter") {
                                  this.handleSubmit();
                                } else {
                                  this.setState({qtext:event.target.value});
                                }
                              }}
          />
      </div>

      <PlayAudio ref='audio' {...this.props.audio}/>

      <RatingOptions
        ref='rating_options'
        {...this.props.rating_options}
        display={this.state.qtype}
        options={this.state.options}/>

      <MultipleChoiceOptions
        ref='multiple_choice_options'
        {...this.props.multiple_choice_options}
        display={this.state.qtype}
        options={this.state.options}/>

      {(this.state.qtype !== 'enter_a_number') ? (<div>
        <h5>Configuration</h5>

        <div className="checkbox">
          <label>
            <input type="checkbox"
                   ref="ordering"
                   checked={this.state.ordering}
                   onChange={(event)=>this.setState({ordering: event.target.checked})}
              /> Preserve ordering of the options
          </label>
        </div>
        <div className="checkbox">
          <label>
            <input type="checkbox"
                   ref="exclusive"
                   checked={this.state.exclusive}
                   onChange={(event)=>this.setState({exclusive: event.target.checked})}
              /> Present options as radio-buttons (default is checkbox)
          </label>
        </div>
        <div className="checkbox">
          <label>
            <input type="checkbox"
                   ref="freetext"
                   checked={this.state.freetext}
                   onChange={(event)=>this.setState({freetext: event.target.checked})}
              /> Allow free text entry
          </label>
        </div>
      </div>) : null}
      {this.props.isDebug ? <button onClick={this.submit}>Submit</button> : null}
    </div>
    );
  }
});
