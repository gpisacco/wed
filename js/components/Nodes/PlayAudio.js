const React = require('react');
const $ = require('jquery');
const FileDropzone = require('react-dropzone');
const PlayStopButton = require('../PlayStopButton');

const AWSAccessKeyId = 'AKIAILECBWDM3STDE34A';

const getTokens = ()=> {
  return new Promise((resolve)=> {
    $.get('http://ec2-54-210-33-204.compute-1.amazonaws.com/workfloweditor/getSignedUrl', (data)=> {
      resolve(JSON.parse(data))
    });
  })
};

const upload = ({file, file_name, cb})=> {
  getTokens()
    .then(({signature, policy})=> {
      const reader = new FileReader();
      reader.onload = function (evt) {
        try {
          const fd = new FormData();
          fd.append('key', file_name);
          fd.append('acl', 'public-read');
          fd.append('Content-Type', 'audio/mp3');
          fd.append('AWSAccessKeyId', AWSAccessKeyId);
          fd.append('policy', policy);
          fd.append('signature', signature);
          fd.append('file', file);
          $.ajax({
            type: 'post',
            url: '//content.lemonadeliveleads.com.s3.amazonaws.com/',
            data: fd,
            processData: false,
            contentType: false,
            xhr: function() {
              // get the native XmlHttpRequest object
              const xhr = $.ajaxSettings.xhr();
              xhr.ctx = this.ctx;
              // set the onprogress event handler
              xhr.upload.onprogress = (evtProgress)=> {
                console.log('progress', evtProgress.loaded / evtProgress.total * 100);
              };
              // set the onload event handler
              xhr.upload.onload = () => {
                cb(file_name);
              };
              // return the customized object
              return xhr;
            },
            ctx: this
          });

        } catch (err) {
          alert("Unable to load survey. Please make sure you upload a " +
          "json file in the correct format.");
          console.error(err);
        }
      };
      reader.readAsArrayBuffer(file);
    })
};

export default React.createClass({
  getInitialState() {
    return {
      src: this.props.src || null
    };
  },
  propTypes: {
    src: React.PropTypes.string,
    handleSubmit: React.PropTypes.func
  },
  handleDrop(files) {
    if (files.length !== 1) {
      throw new Error("Please upload a single file");
    }
    const file = files[0];

    upload({
      file_name: file.name,
      file,
      cb: (file_name)=> {
        this.setState({src: 'http://content.lemonadeliveleads.com/' + file_name})
      }
    });
  },
  submit(){
    if (this.props.handleSubmit) {
      this.props.handleSubmit(this.state)
    }
    return Promise.resolve(this.state)
      .then(()=> this.setState({_label:this.state.src}))
      .then(()=> this.state);
  },
  render() {
    return (
      <div>
        <div className="form-group">
          <label htmlFor="qdrop">Drop File</label>
          <FileDropzone onDrop={this.handleDrop} multiple={false} className="file-dropzone-small">
            <i className="ion-archive"></i>
          </FileDropzone>
        </div>
        <div className="form-group">
          <PlayStopButton src={this.state.src}/>
        </div>
        {this.props.isDebug ? <button onClick={this.submit}>Submit</button> : null}
      </div>
    );
  }
});
