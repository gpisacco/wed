const React = require('react');
const OverlayTrigger = require('react-bootstrap').OverlayTrigger;
const Tooltip = require('react-bootstrap').Tooltip;

const ItemControl = React.createClass({
  propTypes: {
    icon: React.PropTypes.string.isRequired,
    helpText: React.PropTypes.string.isRequired,
    handleClick: React.PropTypes.func.isRequired
  },
  render() {
    const { icon, helpText, handleClick } = this.props;
    const overlay = <Tooltip>{helpText}</Tooltip>;
    return (
      <div onClick={handleClick}>
        <OverlayTrigger
          placement='bottom'
          overlay={overlay}>
          <i className={icon}></i>
        </OverlayTrigger>
      </div>
    );
  }
});

module.exports = ItemControl;
