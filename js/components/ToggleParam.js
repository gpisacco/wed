const React = require('react');
const { Tooltip, OverlayTrigger } = require('react-bootstrap');
const SurveyActions = require('../actions/SurveyActions');

const ToggleParam = React.createClass({
  propTypes: {
    toggleValue: React.PropTypes.bool.isRequired,
    icon: React.PropTypes.string.isRequired,
    helpText: React.PropTypes.string.isRequired,
    toggleName: React.PropTypes.string.isRequired,
    itemType: React.PropTypes.string.isRequired,
    itemId: React.PropTypes.string.isRequired
  },
  handleClick() {
    SurveyActions.toggleParam(
      this.props.itemType,
      this.props.itemId,
      this.props.toggleName
    );
  },
  render() {
    const overlay = <Tooltip>{this.props.helpText}</Tooltip>;
    return (
      <div className={this.props.toggleValue ? 'active' : ''}
           onClick={this.handleClick}>
        <OverlayTrigger
          placement='bottom'
          overlay={overlay}>
          <i className={this.props.icon}></i>
        </OverlayTrigger>
      </div>
    );
  }
});

module.exports = ToggleParam;
