const React = require('react');
const SurveyActions = require('../actions/SurveyActions');
const { List } = require('immutable');

const OptionGroup = require('./OptionGroup');

const OptionGroupArea = React.createClass({
  propTypes: {
    optionGroupId: React.PropTypes.number.isRequired,
    optionGroups: React.PropTypes.instanceOf(List).isRequired
  },
  handleAdd() {
    const labels = prompt("Add new option group (comma separted)");
    const options = labels.split(',').map(l => l.trim());
    SurveyActions.addOptionGroup(options);
  },
  render: function () {
    const { optionGroupId, optionGroups } = this.props;
    return (
      <div>
        <h5>Select / Add Option Lists <i className="ion-plus-circled"
                                         style={{cursor: 'pointer'}} onClick={this.handleAdd}></i></h5>
        <OptionGroup
          options={optionGroups}
          selectedID={optionGroupId}/>
      </div>
    );
  }
});

module.exports = OptionGroupArea;
