const React = require('react');
const Tags = require('react-tag-input').WithOutContext;
const ItemTypes = require('../constants/ItemTypes');
const SurveyActions = require('../actions/SurveyActions');
const SurveyStore = require('../stores/SurveyStore');
const { List } = require('immutable');

const Options = React.createClass({
  propTypes: {
    options: React.PropTypes.instanceOf(List).isRequired,
    questionId: React.PropTypes.string.isRequired
  },
  getInitialState: function () {
    const optionSet = SurveyStore.getOptionsSet();
    return {
      suggestions: Array.from(optionSet)
    };
  },
  handleAddition: function (otext) {
    const optionComponents = otext.split('-');
    const option = {
        number: parseInt(optionComponents[0], 10),
        label: optionComponents[1],
        transition: optionComponents[1],
        id: 'qMultipleChoice-d'+ parseInt(optionComponents[0], 10)
    };
    SurveyActions.optionAdded(this.props.questionId, option);
  },
  handleDoubleClick: function (index) {
  },
  handleDeletion: function (index) {
    const id = this.props.options.getIn([index, 'id']);
    SurveyActions.itemDelete(ItemTypes.OPTION, id);
  },
  handleDrag: function () {
    // TODO: to be implemeeted
  },
  render: function () {
    const options = this.props.options.toJS();
    const options2 = [];
    const numbersConfigured = [];
    for (const j in options) {
      if(options[j].label === undefined || options[j].label === "" ){
          continue;
      }
      const noption = {};
      noption.id = options[j].id;
      noption.otext = options[j].number + "-" + options[j].label + '-' + options[j].transition;
      options2.push(noption);
      numbersConfigured.push(options[j].number);
    }

    const suggestions = [];
    for (let s = 1; s < 10; s++) {
      let add = true;
      for (const n in numbersConfigured) {
        if (parseInt(numbersConfigured[n], 10) == s) {
          add = false;
          break;
        }
      }
      if (add) {
        suggestions.push(s + '-terminate_call');
        suggestions.push(s + '-Go to next question');
      }
    }

    return (
      <Tags tags={options2}
            handleAddition={this.handleAddition}
            suggestions={suggestions}
            handleDelete={this.handleDeletion}
            handleDrag={this.handleDrag}
            labelField={'otext'}
            onDoubleClick={this.handleDoubleClick}/>
    );
  }
});

module.exports = Options;
