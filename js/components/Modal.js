const React = require('react');
const Modal = require('react-bootstrap').Modal;
const Button = require('react-bootstrap').Button;

const ItemTypes = require('../constants/ItemTypes');

const QuestionModal = React.createClass({
  getInitialState() {
    return {
      options: this.props.options,
      show: this.props.isOpen
    };
  },
  componentWillReceiveProps(nextProps) {
    this.setState({
      show: nextProps.isOpen
    });
  },
  propTypes: {
    isOpen: React.PropTypes.bool,
    info: React.PropTypes.object,
    showAlert: React.PropTypes.func.isRequired,
    toggleModal: React.PropTypes.func.isRequired,
    parentID: React.PropTypes.string,
    options: React.PropTypes.array,
    question: React.PropTypes.object,
    question_opts: React.PropTypes.object.isRequired,
    questionUpdated: React.PropTypes.func.isRequired,
    questionDropped: React.PropTypes.func.isRequired
  },
  handleClose() {
    this.props.toggleModal(ItemTypes.QUESTION, this.props.id);
    this.setState({show: false});
    if (this.props.question !== undefined) {
      this.props.question.setState({'changed': true});
    }
  },
  handleSubmit() {
    return this.refs.wrapped_component.submit()
      .then((componentState)=> {
        componentState.id = this.props.id;
        componentState.type = this.props.type;
        componentState.title = this.props.title;
        componentState.parentID = this.props.parentID;

        if (!componentState.id) {
          this.props.questionDropped({data: componentState, type: this.props.type});
        } else {
          this.props.questionUpdated({data: componentState, type: this.props.type});
        }
        this.handleClose();
      }
    )
  },
  render() {
    const WrappedComponent = this.props.modaleComponent;

    return (this.state.show ?<Modal
      onHide={this.handleClose}
      show={this.state.show}
      bsStyle='primary'
      backdrop={true}
      animation={true}
      >
      <Modal.Header>
        <Modal.Title>{this.props.title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className='modal-body'>
          {WrappedComponent ? <WrappedComponent ref='wrapped_component' {...this.props.componentProps} /> : null}
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={this.handleClose}>Close</Button>
        <Button onClick={this.handleSubmit} bsStyle="primary">Save changes</Button>
      </Modal.Footer>
    </Modal> : null);
  }
});

module.exports = QuestionModal;
