const React = require('react');
const { List } = require('immutable');
const Select = require('react-select');
const SurveyActions = require('../actions/SurveyActions');

const OptionGroup = React.createClass({
  propTypes: {
    options: React.PropTypes.instanceOf(List).isRequired,
    selectedID: React.PropTypes.number.isRequired
  },
  handleChange(val) {
    SurveyActions.updateOptionGroup(parseInt(val, 10));
  },
  render() {
    const { options, selectedID } = this.props;
    const selectOptions = options.toJS().map(o => ({
        value: o.id.toString(),
        label: o.optionLabels.join(", ")
      })
    );
    return selectOptions.length === 0 ? null :
      <Select options={selectOptions}
              onChange={this.handleChange}
              clearable={false}
              value={selectedID.toString()}/>;

  }
});

module.exports = OptionGroup;
