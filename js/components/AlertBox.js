const React = require('react');
const Alert = require('react-bootstrap').Alert;
const SurveyActions = require('../actions/SurveyActions');
const AlertTypes = require('../constants/AlertTypes');

// Undo option is not shown for INFO alert level
const AlertBox = React.createClass({
  propTypes: {
    level: React.PropTypes.oneOf([AlertTypes.INFO,
      AlertTypes.WARNING,
      AlertTypes.SUCCESS]).isRequired,
    visible: React.PropTypes.bool.isRequired,
    msg: React.PropTypes.string.isRequired
  },
  handleUndo() {
    SurveyActions.undoSurvey();
  },
  render() {
    if (this.props.visible) {
      return (
        <Alert className="notifications" style={{ zIndex: 10000 }}>
          <p>{this.props.msg}
            { this.props.level === AlertTypes.INFO ? '' :
              <a onClick={this.handleUndo}>Undo</a> }
          </p>
        </Alert>
      );
    }
    return (
      <div></div>
    );
  }
});

module.exports = AlertBox;

