const React = require('react');

export default React.createClass({
  getInitialState() {
    return {isPlaying: false};
  },
  propTypes: {
    src: React.PropTypes.string.isRequired
  },
  handlePlayAudio() {
    const audio = React.findDOMNode(this.refs.player);
    const newIsPlaying = !this.state.isPlaying;
    this.setState({isPlaying: newIsPlaying});
    if (newIsPlaying) {
      audio.play();
    } else {
      audio.pause();
      audio.currentTime = 0;
    }
  },
  render() {
    return (
      <div>
        {this.props.src ? (<div><audio src={this.props.src} preload="false" id="player" ref="player"/>
        <div className="checkbox" onClick={this.handlePlayAudio}>
          <label> <i className={this.state.isPlaying?"ion-stop" : "ion-play"}></i> {this.file_name}</label>
        </div></div>): null}
      </div>
    );
  }
});
