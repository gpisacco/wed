const React = require('react');
const Modal = require('react-bootstrap').Modal;
const Button = require('react-bootstrap').Button;
const OverlayMixin = require('react-bootstrap').OverlayMixin;
const SurveyActions = require('../actions/SurveyActions');
const LoadSurveyModalBaseModal = require('./LoadSurveyBaseModal');

const LoadSurveyModal = React.createClass({
  mixins: [OverlayMixin],
  propTypes: {
    isOpen: React.PropTypes.bool.isRequired,
    savedSurveys: React.PropTypes.array.isRequired
  },
  loadSurvey(survey){

    SurveyActions.loadSurvey(survey);
  },
  toggleLoadModal(survey){
    SurveyActions.toggleLoadModal(survey);
  },
  render() {
    return (
      <div className='static-modal'></div>
    );
  },
  renderOverlay() {
    if (!this.props.isOpen) {
      return <div></div>;
    }
    return (
      <LoadSurveyModalBaseModal
        loadSurvey={this.loadSurvey}
        savedSurveys={this.props.savedSurveys}/>
    );
  }
});

module.exports = LoadSurveyModal;
