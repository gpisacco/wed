const React = require('react');
const Modal = require('react-bootstrap').Modal;
const Button = require('react-bootstrap').Button;
const FileDropzone = require('react-dropzone');

// to get around the refs issues - https://github.com/react-bootstrap/react-bootstrap/issues/223
const LoadSurveyModalBaseModal = React.createClass({
  propTypes: {
    savedSurveys: React.PropTypes.object.isRequired
  },
  handleClose() {
    this.props.toggleLoadModal();
  },
  handleDrop(files) {
    if (files.length !== 1) {
      throw new Error("Please upload a single file");
    }
    const file = files[0];
    const reader = new FileReader();
    reader.onload = (evt) => {
      try {
        const { survey } = JSON.parse(evt.target.result);
        this.props.loadSurvey(survey);
        this.props.toggleLoadModal();
      } catch (err) {
        alert("Unable to load survey. Please make sure you upload a " +
        "json file in the correct format.");
        console.error(err);
      }
    };
    reader.readAsText(file, "UTF-8");
  },
  handleClick(index) {
    const { data } = this.props.savedSurveys[index];
    this.props.loadSurvey(data);
    this.props.toggleLoadModal();
  },
  render() {
    const { savedSurveys } = this.props;
    const saves = savedSurveys.map((s, i) =>
        <li key={i}>
          <a title={"Saved on: " + new Date(s.createdAt)}
             onClick={this.handleClick.bind(this, i)}>
            {s.title}
          </a>
        </li>
    );

    return (
      <Modal title='Load Survey' bsStyle='warning' backdrop={true}
             animation={true} container={null} closeButton={false}
             onRequestHide={this.handleClose}>
        <div className='modal-body'>
          <h3>Saved Surveys</h3>

          <p>Here are your saved surveys that we have found. Click on any to load into the pallet.</p>

          {saves.length === 0 ? <p>No saved surveys found</p> : <ul> {saves} </ul>}

          <h2><span>OR</span></h2>

          <h3>Upload a Survey file</h3>

          <p>Try dropping some files here, or click to select files to upload.</p>
          <FileDropzone onDrop={this.handleDrop} multiple={false} className="file-dropzone">
            <i className="ion-archive"></i>
          </FileDropzone>
        </div>
        <div className='modal-footer'>
          <Button onClick={this.handleClose}>Cancel</Button>
        </div>
      </Modal>
    );
  }
});

module.exports = LoadSurveyModalBaseModal;
