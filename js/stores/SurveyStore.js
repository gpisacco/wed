const Reflux = require('reflux');
const Immutable = require('immutable');
const Lockr = require('lockr');
const React = require('react');
import R from 'ramda'
const initialData = require('../data.js');

const SurveyActions = require('../actions/SurveyActions');
const AlertTypes = require('../constants/AlertTypes');
const ItemTypes = require('../constants/ItemTypes');

// TODO delete this use real data
import campaigns from '../../tests/data/campaigns.json'
import numbers from '../../tests/data/numbers.json'
import pools from '../../tests/data/pools.json'

const prepare = R.compose(R.map(({_id, name})=>({value:_id, label:name})), ({_items})=>_items);
const prepareNumbers = R.map(({_id:{$oid}, number})=>({value:$oid, label:number}));


// a set of option texts - helps in generating suggestions
let _optionsSet = Immutable.OrderedSet();

// initialize question, option, block maps that will help in
// faster retrieval of associated blocks and questions.
let _questionMap = Immutable.Map();     // questionId => blockId
let _optionMap = Immutable.Map();       // optionId   => questionId
let _blockMap = Immutable.Map();        // subblockId => blockid

// CONSTS
const ALERT_TIMEOUT = 5 * 1000; // toggles how quickly the alert hides
const LOCALSTORAGE_KEY = 'savedSurveyList'; // the key against which the survey data is stored in LS

// mananging history
const _history = [];

const SurveyStore = Reflux.createStore({
  listenables: [SurveyActions],
  data: {
    surveyData: Immutable.List(),
    modalState: Immutable.Map({
      dropTargetID: null,
      isOpen: false,
      info: null
    }),
    numbers:prepareNumbers(numbers),
    pools:prepare(pools),
    campaigns:prepare(campaigns),
    loadSurveyModalState: false,
    savedSurveys: [],
    alertState: Immutable.Map({
      msg: '',
      level: AlertTypes.INFO,
      visible: false
    }),
    optionGroupState: Immutable.Map({
      selectedID: 1,
      options: Immutable.List()
    })
  },
  // called when the app component is loaded
  init() {
    const initOptionsData = Immutable.fromJS([
      {id: 0, optionLabels: ["Yes", "No"]},
      {id: 1, optionLabels: ["True", "False"]},
      {id: 2, optionLabels: ["Strongly Disagree", "Disagree", "Neither agree or disagree", "Agree", "Strongly Agree"]}
    ]);

    this.listenTo(SurveyActions.load, () => {
      window.location.hash = "";   // clear the location hash on app init

      this.data.optionGroupState = this.data.optionGroupState.set('options', initOptionsData);

      // read the saved survey data
      this.data.savedSurveys = Lockr.get(LOCALSTORAGE_KEY) || [];

      // load up survey data
      const data = Immutable.fromJS(initialData);
      this.updateSurveyData(data, true);
    });
  },
  getInitialState() {
    return {
      surveyData: this.data.surveyData,
      modalState: this.data.modalState,
      alertState: this.data.alertState,
      optionGroupState: this.data.optionGroupState,
      loadSurveyModalState: this.data.loadSurveyModalState,
      savedSurveys: this.data.savedSurveys
    };
  },
  /**
   * Updates the survey data as the args provided. Triggers refresh.
   * Stores prev state in history, if second param is true
   * @param data surveydata
   * @param cache - boolean
   */
    updateSurveyData(data, cache = false) {
    if (cache) {
      _history.push({
        data: this.data.surveyData,
        optionMap: _optionMap.toJS(),
        questionMap: _questionMap.toJS()
      });
    }
    this.data.surveyData = data;
    this.trigger(this.data);
  },
  /*
   * Returns the set (unique list) of options.
   */
  getOptionsSet() {
    return _optionsSet;
  },
  /*
   * Returns the surveyjson data
   */
  getSurveyData() {
    return this.data.surveyData.toJS();
  },
  /**
   * Returns the id of the block which has the
   * question with questionId
   * @param questionId
   */
    getBlockId(questionId) {
    return _questionMap.get(questionId);
  },
  /**
   * Returns a new ID based on the type of object requested
   * @param type one of ItemTypes.OPTION, ItemTypes.BLOCK, ItemTypes.QUESTION
   */
    getNewId(type) {
    let prefix;
    if (type === ItemTypes.QUESTION) {
      prefix = "q";
    } else if (type === ItemTypes.OPTION) {
      prefix = "o";
    } else {
      prefix = "b";
    }
    return `${prefix}_${Math.floor((Math.random() * 99999) + 1)}`;
  },
  /**
   * Helper function that returns the complete path to the block requested.
   * @param blockID id of the block
   * @param survey source survey object in which the block is to be found
   * @param blockMap (optional) the block map to use for lookup
   */
    getBlockPath(blockID, survey, blockMap = _blockMap) {
    // function that returns a chain of IDs from the root block
    // to the block with id - id
    const getIDsList = function getIDsList(id, path = []) {
      if (!blockMap.has(id)) {
        return path.concat([id]).reverse();
      }
      return getIDsList(blockMap.get(id), path.concat([id]));
    };

    // function that returns the index of block id within the list of blocks
    const getIndex = (id, list) => list.findIndex(b => b.get('id') === id);

    // initialize path with index of root node
    const [rootID, ...restIDs] = getIDsList(blockID);
    const initPath = [getIndex(rootID, survey)];

    // reduce over the rest of ids by finding id at each level
    // and changing path accordingly
    return restIDs.reduce((path, id) => {
      const newPath = path.concat(['subblocks']);
      const index = getIndex(id, survey.getIn(newPath));
      return newPath.concat([index]);
    }, initPath);
  },
  /**
   * Helper function that returns the complete path to the question requested.
   * @param questionID id of the question
   * @param survey source survey object in which the question is to be found
   * @param questionMap (optional) the question map to use for lookup
   */
    getQuestionPath(questionID, survey, questionMap = _questionMap) {
    const blockPath = this.getBlockPath(questionMap.get(questionID), survey);
    const index = survey.getIn([...blockPath, 'questions']).findIndex(q => {
      return q.get('questionData').get('id') === questionID});
    return [...blockPath, 'questions', index];
  },
  /**
   * Runs when the blockDropped action is called by the view.
   * Adds a new block to the end of the survey object.
   * @param targetID: targetId of the target on which the block is dropped.
   * If this is undefined, then block is assumed to have dropped on the survey
   */
    onBlockDropped(targetID) {
    const survey = this.data.surveyData;
    const newBlock = Immutable.fromJS({
      id: this.getNewId(ItemTypes.BLOCK),
      questions: [],
      subblocks: [],
      randomize: true
    });

    if (targetID === undefined) {
      // block is dropped on the survey
      const newSurvey = survey.splice(0, 0, newBlock);

      // update and cache
      this.updateSurveyData(newSurvey, true);
      SurveyActions.showAlert("New block added.", AlertTypes.SUCCESS);
    } else {
      const blockPath = this.getBlockPath(targetID, survey);
      const newSurvey = survey.updateIn([...blockPath, 'subblocks'],
          list => list.splice(0, 0, newBlock)
      );
      this.updateSurveyData(newSurvey, true);

      // update block map with new subblock
      _blockMap = _blockMap.set(newBlock.get('id'), targetID);
      SurveyActions.showAlert("New subblock added.", AlertTypes.SUCCESS);
    }
  },
  /**
   * Runs when the optiongroup is dropped on a question
   * @param questionId - ID of the question on which
   * the option group is dropped
   */
    onOptionGroupDropped(questionId) {
    const selectedID = this.data.optionGroupState.get('selectedID');
    const optionLabels = this.data.optionGroupState
      .getIn(['options', selectedID, 'optionLabels']);
    optionLabels.forEach(op => this.onOptionAdded(questionId, op));
  },
  /**
   * Runs when the questionDropped action is called by the view.
   * Adds a question to the block who's id is provided as param
   * @param questionObj A POJO containing data the for the new question.
   * with the following keys - parentID, qtext, config
   */
    onQuestionDropped(questionObj) {

    const questionData = questionObj.data;
    const questionType = questionObj.type;
    const survey = this.data.surveyData;
    questionData.id = this.getNewId(ItemTypes.QUESTION);
    const newQuestion = Immutable.fromJS({
      questionData: questionData,
      questionType: questionType,
      options: [],
      id: questionData.id
    });

    const blockPath = this.getBlockPath(questionData.parentID, survey);
    const newSurvey = survey.updateIn([...blockPath, 'questions'],
        list => list.splice(0, 0, newQuestion)
    );

    // update and cache
    this.updateSurveyData(newSurvey, true);
    const options = questionData.options;
    for (const i in options) {
      SurveyActions.questionData(newQuestion.id, options[i]);
    }
    // update question map with new question
    const block = survey.getIn(blockPath);
    _questionMap = _questionMap.set(questionData.id, block.get('id'));

    SurveyActions.showAlert("New Question added.", AlertTypes.SUCCESS);
  },
  /**
   * Runs when the questionUpdated action is called by the view.
   * Updates a question to the block who's id is provided as param
   * @param questionObj A POJO containing data the for the new question.
   * with the following keys - parentID, qtext, config
   */
    onQuestionUpdated(questionObj) {

    const questionData = questionObj.data;
    const questionType = questionObj.type;

    var survey = this.data.surveyData;

    var qid = questionData.id;
    var options = questionData.options;
    for (var i  in  options) {
      SurveyActions.optionAdded(qid, options[i]);
    }

    var path = this.getQuestionPath(qid, survey);
    var newSurvey = survey.updateIn(path, q => q.set('questionData', Immutable.fromJS(questionData)));
    this.updateSurveyData(newSurvey, true);

    SurveyActions.showAlert("Question updated.", AlertTypes.SUCCESS);
  },
  /**
   * Runs when the optionAdded action is called by the view.
   * Adds an option with text as otext to the question whose id is provided as an argument.
   * @param questionId (int) of the question to which the option will be added.
   * @param otext (string) the text of the option to be added
   */
    onOptionAdded(questionId, otext) {
    let oid = this.getNewId(ItemTypes.OPTION);
    if (otext.id !== undefined) {
      oid = otext.id;
    }
    // template for new Option
    const newOption = Immutable.Map({
      id: oid,
      label: otext.label,
      transition: otext.transition,
      number: otext.number,
    });

    const survey = this.data.surveyData;
    const questionPath = this.getQuestionPath(questionId, survey);
    let newSurvey = survey;
    if (otext.id === undefined) {
      newSurvey = survey.updateIn([...questionPath, 'options'],
          list => list.push(newOption)
      );
    }

    // update the option map and options set
    _optionMap = _optionMap.set(newOption.get('id'), questionId);
    _optionsSet = _optionsSet.add(otext);

    this.updateSurveyData(newSurvey, false);
  },
  /**
   * Run when the action toggleModal is called by the view
   * @param modalType - Refer to the type of object that was dropped
   * @param dropTargetID - Refers to the ID on which the object was dropped
   */
    onToggleModal(modalType, dropTargetID, question) {
    let modalState = this.data.modalState;

    // TODO: this handles the modal for question separately, although
    // this is not really required. deal with it later.
    if (modalType === ItemTypes.QUESTION) {
      modalState = modalState.set('isOpen', !modalState.get('isOpen'));
    }

    // sets the correct dropTarget to pass down to component
    this.data.modalState = modalState.set('dropTargetID', dropTargetID);
    if (question != undefined) {
      this.data.modalState = modalState.set('info', question);
    } else {
      this.data.modalState = modalState.set('info', null);
    }
    this.trigger(this.data);
  },
  /**
   * Run when the action showAlert is called. Responsible for displaying
   * alert in the app
   * @param msg - the msg to be displayed
   * @param level - the level. defaults to 'info'. See Bootstrap alerts for more.
   */
    onShowAlert(msg, level = AlertTypes.INFO) {
    this.data.alertState = Immutable.Map({
      msg: msg,
      level: level,
      visible: true
    });
    this.trigger(this.data);

    // Hides the alert box
    setTimeout((self) => {
      self.data.alertState = self.data.alertState.set('visible', false);
      self.trigger(self.data);
    }, ALERT_TIMEOUT, this);
  },
  /**
   * Called when the clearSurvey action is called.
   * Clears up the existing survey state, takes a copy and allows the user
   * to start afresh.
   */
    onClearSurvey() {
    const data = Immutable.fromJS(initialData);
    this.updateSurveyData(data, true);

    SurveyActions.showAlert("New survey created", AlertTypes.SUCCESS);

    // clear up the maps and set
    _questionMap = Immutable.Map();
    _optionMap = Immutable.Map();
    _blockMap = Immutable.Map();
    _optionsSet = Immutable.OrderedSet();
  },
  /**
   * Called when the saveSurvey action is called.
   * Stores a snapshot of the survey JSON object in the localStorage.
   */
    onSaveSurvey(surveyTitle) {
    const newSurvey = {
      title: surveyTitle,
      data: this.data.surveyData.toJS(),
      createdAt: Date.now()
    };
    const savedSurveys = Lockr.get(LOCALSTORAGE_KEY) || [];
    Lockr.set(LOCALSTORAGE_KEY, savedSurveys.concat([newSurvey]));

    // update the cached survey data to include the latest
    this.data.savedSurveys = Lockr.get(LOCALSTORAGE_KEY);

    SurveyActions.showAlert("Survey saved!", AlertTypes.INFO);
  },
  /**
   * Called when the loadSurvey action is triggered.
   * Takes the survey json as param and loads that into the application state.
   * @param rawData survey data in json
   */
    onLoadSurvey(rawData) {
    var el = 
        {
          "id": "b_10101",
          "randomize": true,
          "questions": [
            {
                "id": "q_48493",
            
              "questionData": {
                "parentID": null,
                "qtype": "enter_a_number",
                "_label": "Please Enter your 10 Digits Number",
                "title": "Question",
                "showAudio": "none",
                "type": "QUESTION",
                "id": "q_48493",
                "rating_options": {
                  "rating": 0
                },
                "qtext": "Please Enter your 10 Digits Number",
                "options": [
                  {
                    "number": 1,
                    "label": ""
                  },
                  {
                    "number": 2,
                    "label": ""
                  },
                  {
                    "number": 3,
                    "label": ""
                  },
                  {
                    "number": 4,
                    "label": ""
                  },
                  {
                    "number": 5,
                    "label": ""
                  },
                  {
                    "number": 6,
                    "label": ""
                  },
                  {
                    "number": 7,
                    "label": ""
                  },
                  {
                    "number": 8,
                    "label": ""
                  },
                  {
                    "number": 9,
                    "label": ""
                  }
                ]
              },
              "questionType": "QUESTION",
              "options": []
            },
            {            "id": "q_54878",

              "questionData": {
                "parentID": null,
                "qtype": "multiple_choice",
                "_label": "Do you want to receive a $5 coupon?",
                "title": "Question",
                "showAudio": "none",
                "type": "QUESTION",
                "id": "q_54878",
                "rating_options": {
                  "rating": 0
                },
                "qtext": "Do you want to receive a $5 coupon?",
                "options": [
                  {
                    "label": "",
                    "number": 0,
                    "id": "qMultipleChoice-d0"
                  },
                  {
                    "transition": "next_question",
                    "label": "Yes",
                    "number": 1,
                    "id": "qMultipleChoice-d1"
                  },
                  {
                    "transition": "terminate_call",
                    "label": "No",
                    "number": 2,
                    "id": "qMultipleChoice-d2"
                  },
                  {
                    "label": "",
                    "number": 3,
                    "id": "qMultipleChoice-d3"
                  },
                  {
                    "label": "",
                    "number": 4,
                    "id": "qMultipleChoice-d4"
                  },
                  {
                    "label": "",
                    "number": 5,
                    "id": "qMultipleChoice-d5"
                  },
                  {
                    "label": "",
                    "number": 6,
                    "id": "qMultipleChoice-d6"
                  },
                  {
                    "label": "",
                    "number": 7,
                    "id": "qMultipleChoice-d7"
                  },
                  {
                    "label": "",
                    "number": 8,
                    "id": "qMultipleChoice-d8"
                  }
                ]
              },
              "questionType": "QUESTION",
              "options": []
            },
            {            "id": "q_53408",

              "questionData": {
                "parentID": null,
                "qtype": "multiple_choice",
                "_label": "Is this a mobile phone?",
                "title": "Question",
                "showAudio": "none",
                "type": "QUESTION",
                "id": "q_53408",
                "rating_options": {
                  "rating": 0
                },
                "qtext": "Is this a mobile phone?",
                "options": [
                  {
                    "label": "",
                    "number": 0,
                    "id": "qMultipleChoice-d0"
                  },
                  {
                    "transition": "next_question",
                    "label": "Yes",
                    "number": 1,
                    "id": "qMultipleChoice-d1"
                  },
                  {
                    "transition": "next_question",
                    "label": "No",
                    "number": 2,
                    "id": "qMultipleChoice-d2"
                  },
                  {
                    "label": "",
                    "number": 3,
                    "id": "qMultipleChoice-d3"
                  },
                  {
                    "label": "",
                    "number": 4,
                    "id": "qMultipleChoice-d4"
                  },
                  {
                    "label": "",
                    "number": 5,
                    "id": "qMultipleChoice-d5"
                  },
                  {
                    "label": "",
                    "number": 6,
                    "id": "qMultipleChoice-d6"
                  },
                  {
                    "label": "",
                    "number": 7,
                    "id": "qMultipleChoice-d7"
                  },
                  {
                    "label": "",
                    "number": 8,
                    "id": "qMultipleChoice-d8"
                  }
                ]
              },
              "questionType": "QUESTION",
              "options": []
            }
          ],
          "subblocks": []
        }
    ;
    rawData = [];
    rawData.push(el);
    // update the survey object
    const data = Immutable.fromJS(rawData);
    this.updateSurveyData(data, true);

    // clear up the maps & build them from scratch
    _questionMap = Immutable.Map();
    _optionMap = Immutable.Map();
    _blockMap = Immutable.Map();
    data.forEach((block) => this.buildMapsForBlock(block));

    SurveyActions.showAlert("Survey loaded.", AlertTypes.SUCCESS);
  },
  /**
   * Called when the toggleLoadModal action is triggered.
   * Toggles the visibility of the load survey modal.
   */
  onToggleLoadModal() {
    this.data.loadSurveyModalState = !this.data.loadSurveyModalState;
    this.trigger(this.data);
    console.log(this.data);
  },
  /**
   * Takes a block and runs over its children recursively and
   * populates the maps (option, question, block) with correct mappings
   * @param block - Block of type Immutable.Map
   */
    buildMapsForBlock(block) {
    const blockId = block.get('id');
    _optionsSet = Immutable.OrderedSet();

    // start with the questions
    block.get('questions').forEach((q) => {
      const qId = q.get('id');
      _questionMap = _questionMap.set(qId, blockId);

      // tackle the questions
      q.get('options').forEach((o) => {
        _optionMap = _optionMap.set(o.get('id'), qId);
        _optionsSet = _optionsSet.add(o.get('otext'));
      });
    });

    // handle subblocks
    block.get('subblocks').forEach((b) => {
      _blockMap = _blockMap.set(b.get('id'), blockId);

      // call this recursively for each subblock
      this.buildMapsForBlock(b);
    });
  },
  /**
   * Returns an array of indices that can be directly go in first arguments to Immutable deep persistent functions.
   * @param blockId - id of the block who's index is required
   * @param parentBlock (optional) - The parent block at which to begin the search. If left out, the search starts from the top of the survey
   * within the `parentBlock`
   */
    getBlockIndex(blockId, parentBlock = false) {
    // find in the survey
    if (!parentBlock) {
      return this.data.surveyData.findIndex(b => b.get('id') === blockId);
    }
    return parentBlock.get('subblocks').findIndex(b => b.get('id') === blockId);
  },
  /**
   * Returns the index of a question in a block
   * @param id - id of the question
   * @param block - obj (Immutable.Map) of the container block
   */
    getQuestionIndex(questionId, block) {
    return block.get('questions').findIndex(q => q.get('id') === questionId);
  },
  /**
   * Called when the toggleParam action is called.
   * Toggles the property on the item.
   * @param itemType - type of Item the toggle button is clicked. one of ItemTypes
   * @param itemId - Id of the item for which toggle button is clicked
   * @param toggleName - string name of property that is toggled.
   */
    onToggleParam(itemType, itemId, toggleName) {
    const survey = this.data.surveyData;

    // handle the case when a param on a block is toggled
    if (itemType === ItemTypes.BLOCK) {
      const blockPath = this.getBlockPath(itemId, survey);
      const newSurvey = survey.updateIn(blockPath,
          b => b.set(toggleName, !b.get(toggleName))
      );
      this.updateSurveyData(newSurvey);
    }

    // handle the case when a param on a question is toggled
    else if (itemType === ItemTypes.QUESTION) {
      const questionPath = this.getQuestionPath(itemId, survey);
      const newSurvey = survey.updateIn(questionPath,
          q => q.set(toggleName, !q.get(toggleName))
      );
      this.updateSurveyData(newSurvey);
    }

    // throw exception
    else {
      throw new Error("Not a valid item type");
    }
  },
  /**
   * Returns a clone of Question passed as a parameter.
   * Updates _optionMap with all new options
   * @param question - type of Immutable.Map. The question to be cloned
   */
    cloneQuestion(question) {
    const self = this;

    // get a new ID
    let newQuestion = question.set('id', self.getNewId(ItemTypes.QUESTION));

    // for each option, get a new option but with new ID and same otext
    newQuestion = newQuestion.update('options',
      (list) => list.map(o => Immutable.Map({
        id: self.getNewId(ItemTypes.OPTION),
        otext: o.get('otext')
      }))
    );

    const qId = newQuestion.get('id');
    newQuestion.get('options').forEach(o => {
      _optionMap = _optionMap.set(o.get('id'), qId);
    });
    return newQuestion;
  },
  /**
   * Returns a clone of Block passed as a parameter.
   * Updates _blockMap and _questionMap with the new subblocks and questions
   * @param block - type of Immutable.Map. The block to be cloned.
   */
    cloneBlock(block) {
    const self = this;
    const newBlock = block
      .set('id', self.getNewId(ItemTypes.BLOCK))
      .update('questions', (list) => list.map(ques => self.cloneQuestion(ques)))
      .update('subblocks', (list) => list.map(blk => self.cloneBlock(blk)));

    const blockId = newBlock.get('id');

    newBlock.get('questions').forEach(q => {
      _questionMap = _questionMap.set(q.get('id'), blockId);
    });
    newBlock.get('subblocks').forEach(b => {
      _blockMap = _blockMap.set(b.get('id'), blockId);
    });
    return newBlock;
  },
  /**
   * Method called when the itemCopy action is triggered.
   * Responsible for creating a new copy of an ItemType - works only for
   * question and block.
   * @param itemType type of ItemType
   * @param itemId id of the item to be cloned
   */
    onItemCopy(itemType, itemId) {
    const survey = this.data.surveyData;

    if (itemType === ItemTypes.BLOCK) {
      const blockPath = this.getBlockPath(itemId, survey);
      const blockIndex = blockPath[blockPath.length - 1];
      const newBlock = this.cloneBlock(survey.getIn(blockPath));
      let newSurvey;
      if (_blockMap.has(itemId)) { // if subblock, append in parent
        const parentId = _blockMap.get(itemId);
        const parentblockPath = this.getBlockPath(parentId, survey);
        newSurvey = survey.updateIn([...parentblockPath, 'subblocks'],
            list => list.splice(blockIndex + 1, 0, newBlock)
        );
        _blockMap = _blockMap.set(newBlock.get('id'), parentId);
      } else { // else simply put in survey array
        newSurvey = survey.splice(blockIndex + 1, 0, newBlock);
      }

      // update
      this.updateSurveyData(newSurvey, false);

      // alert and focus
      SurveyActions.showAlert("Block copied.", AlertTypes.INFO);
      SurveyActions.scrollToItem(newBlock.get('id'));
    }

    else if (itemType === ItemTypes.QUESTION) {
      const questionPath = this.getQuestionPath(itemId, survey);
      const questionIndex = questionPath[questionPath.length - 1];
      const newQuestion = this.cloneQuestion(survey.getIn(questionPath));
      const newSurvey = survey.updateIn(questionPath.slice(0, -1),
          list => list.splice(questionIndex + 1, 0, newQuestion)
      );
      _questionMap = _questionMap.set(newQuestion.get('id'), _questionMap.get(itemId));

      // update and cache
      this.updateSurveyData(newSurvey, false);

      // alert and focus
      SurveyActions.showAlert("Question copied.", AlertTypes.INFO);
      SurveyActions.scrollToItem(newQuestion.get('id'));
    }

    else {
      throw new Error("Not a valid item type");
    }
  },
  /**
   * Called when an item has to be deleted.
   * @param itemType - refers to the type of item to be deleted. One of ItemTypes.
   * @param itemId - Id of item to be deleted.
   */
    onItemDelete(itemType, itemId) {
    const survey = this.data.surveyData;

    // handle block delete
    if (itemType === ItemTypes.BLOCK) {
      const blockPath = this.getBlockPath(itemId, survey);
      const newSurvey = survey.deleteIn(blockPath);

      this.updateSurveyData(newSurvey, true);

      // delete the mapping of question and options
      survey.getIn([...blockPath, 'questions']).forEach(q => {
        _questionMap = _questionMap.delete(q.get('id'));
        q.get('options').forEach(o => {
          _optionMap = _optionMap.delete(o.get('id'));
        });
      });

      SurveyActions.showAlert("Block deleted successfully.", AlertTypes.SUCCESS);
    }

    // handle question delete
    else if (itemType === ItemTypes.QUESTION) {
      const questionPath = this.getQuestionPath(itemId, survey);
      const newSurvey = survey.deleteIn(questionPath);

      // update and cache
      this.updateSurveyData(newSurvey, true);

      // delete the mapping of the question and its options
      _questionMap = _questionMap.delete(itemId);
      survey.getIn([...questionPath, 'options']).forEach(o => {
        _optionMap = _optionMap.delete(o.get('id'));
      });

      SurveyActions.showAlert("Question deleted successfully.", AlertTypes.SUCCESS);
    }

    // handle option delete
    else if (itemType === ItemTypes.OPTION) {
      const questionPath = this.getQuestionPath(_optionMap.get(itemId), survey);
      const index = survey.getIn([...questionPath, 'options'])
        .findIndex(op => op.get('id') === itemId);

      const newSurvey = survey.deleteIn([...questionPath, 'options', index]);

      // delete the mapping
      _optionMap = _optionMap.delete(itemId);
      this.updateSurveyData(newSurvey);
    }

    // throw exception
    else {
      throw new Error("Not a valid item type");
    }
  },
  /**
   * Called when the question text is edited. Sets qtext to new value.
   * @param text - new text value
   * @param questionId - id of the question that needs to be changed
   */
    onSaveEditText(text, questionId) {
    const survey = this.data.surveyData;
    const path = this.getQuestionPath(questionId, survey);
    const newSurvey = survey.updateIn(path, q => q.set('qtext', text));
    this.updateSurveyData(newSurvey, true);
  },
  /**
   * Used to tag on a string value of freeText to a question
   * @param text The value of freetext
   * @param questionId Id of the question to which the freetext prop should be added
   */
    onSaveFreeText(text, questionId) {
    const survey = this.data.surveyData;
    const path = this.getQuestionPath(questionId, survey);
    const newSurvey = survey.updateIn(path, q => q.set('freetext', text));
    this.updateSurveyData(newSurvey, true);
  },
  /**
   * Called when the undoSurvey action is triggered. Responsible for
   * setting global state to last _history item.
   */
    onUndoSurvey() {
    // hide the alert
    this.data.alertState = this.data.alertState.set('visible', false);

    // retrieve cached data
    const { data, optionMap, questionMap } = _history.pop();
    _questionMap = Immutable.Map(questionMap);
    _optionMap = Immutable.Map(optionMap);
    this.updateSurveyData(data);
  },
  /**
   * Called when the scrolltoItem action is triggered. Scrolls the item
   * into view
   * @param id - id of the item that needs to be scrolled to
   */
    onScrollToItem(id) {
    window.location.hash = id;
  },
  // called when a new optiongroup is selected as the default in the optionlist selectbox
  onUpdateOptionGroup(id) {
    this.data.optionGroupState = this.data.optionGroupState.set('selectedID', id);
    this.trigger(this.data);
  },
  /**
   * @param options - array of options
   */
    onAddOptionGroup(options) {
    const { optionGroupState } = this.data;
    const newId = optionGroupState.get('options').count();
    this.data.optionGroupState = optionGroupState
      .set('selectedID', newId)
      .updateIn(['options'], list => list.push(
        Immutable.Map({id: newId, optionLabels: options})
      ));
    this.trigger(this.data);
  },
  onMoveQuestion(questionID, blockID) {
    const survey = this.data.surveyData;
    const currBlockID = _questionMap.get(questionID);

    // if the question is dropped in the same block then do nothing
    if (currBlockID === blockID) {
      return;
    }

    const questionPath = this.getQuestionPath(questionID, survey);
    const newBlockPath = this.getBlockPath(blockID, survey);
    const question = survey.getIn(questionPath);

    const newSurvey = survey.deleteIn(questionPath)
      .updateIn([...newBlockPath, 'questions'], list => list.push(question));

    // update and cache
    this.updateSurveyData(newSurvey, true);

    // update the mappings of the question
    _questionMap = _questionMap.set(questionID, blockID);

    SurveyActions.showAlert("Question moved.", AlertTypes.SUCCESS);
  },
  /**
   * Called when an item is dragged to be re-ordered in the treeview.
   * This works on the assumption that the item is ordered within its parent container.
   * @param draggedItemId: id of the block being dragged
   * @param finalIndex: final location where the item needs to be moved to within the container
   */
    onReorderItem(draggedItemId, finalIndex, itemType) {
    const survey = this.data.surveyData;

    if (itemType === ItemTypes.BLOCK) {
      const draggedBlockIndex = this.getBlockIndex(draggedItemId);
      const block = survey.get(draggedBlockIndex);
      const newSurvey = survey.delete(draggedBlockIndex).splice(finalIndex, 0, block);
      this.updateSurveyData(newSurvey, false);
    }
    else if (itemType === ItemTypes.QUESTION) {
      const draggedBlockId = _questionMap.get(draggedItemId);
      const draggedBlockIndex = this.getBlockIndex(draggedBlockId);
      const block = survey.get(draggedBlockIndex);
      const draggedQuestionIndex = this.getQuestionIndex(draggedItemId, block);
      const draggedQuestion = block.getIn(['questions', draggedQuestionIndex]);
      const newSurvey = survey.updateIn([draggedBlockIndex, 'questions'],
        (questions) => questions.delete(draggedQuestionIndex)
          .splice(finalIndex, 0, draggedQuestion)
      );
      this.updateSurveyData(newSurvey, false);
    }
    else {
      throw new Error('Invalid item type');
    }
  },
  onPopulateSelect(target) {
    var options = target.state.selectOptions;
    const questions = this.data.surveyData.toJS()[0].questions;
    for (let i = 0; i < questions.length; i++) {
      options.push({
        label: "Go to " + questions[i].questionData.qtext,
        value: questions[i].id,
      });
    }
    target.setState({
        selectOptions: options
    });
    target.render();
    
  }
});

module.exports = SurveyStore;
