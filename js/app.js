const React = require("react");
const Immutable = require("immutable");
require('../css/main.css');
require('../css/vendor/bootstrap/css/bootstrap.min.css');
require("babel/polyfill");

const App = require("./containers/Application");
const AppComponent = React.createFactory(App);

React.render(AppComponent(), document.getElementById("app"));