var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

var plugins = [ new ExtractTextPlugin("build/styles.min.css", { allChunks: true }) ];

if (process.env.NODE_ENV === "production") {
    plugins.push( new webpack.DefinePlugin({'process.env': {NODE_ENV: '"production"'}}) );
}

module.exports = {
    entry: "./js/app.js",
    devtool:'cheap-module-eval-source-map',
    module: {
        loaders: [
            { 
                test: /\.js$/, 
                loader: 'babel', 
                exclude: /node_modules/ 
            },
            {   
                test: /\.css$/, 
                loader: ExtractTextPlugin.extract('style-loader', "css-loader") 
            },
            {
                test: /\.woff(2)?(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&mimetype=application/font-woff"
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&mimetype=application/octet-stream"
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file"
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&mimetype=image/svg+xml"
            },
            {
                test: /\.json$/,
                loader: "json"
            }
        ]
    },
    plugins: plugins,
    output: {
        filename: "build/bundle.js"
    }
};
