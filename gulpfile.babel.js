import gulp  from 'gulp';
import mocha  from 'gulp-mocha';
import gutil  from 'gulp-util';
import cliClear  from 'cli-clear';
import gulpSequence  from 'gulp-sequence';
import notify  from "gulp-notify";
import eslint  from 'gulp-eslint';
import friendlyFormatter from "eslint-friendly-formatter";

const testsDir = 'tests/**/*.js';
const srcDir = 'js/**/*.js';

gulp.task('cli-clear', ()=> cliClear());

gulp.task('lint', () =>(
  gulp.src([srcDir, testsDir])
    .pipe(eslint())
    .pipe(eslint.formatEach(friendlyFormatter))
));

gulp.task('test', () =>(
  gulp.src([testsDir])
    .pipe(mocha({
        reporter: 'spec',
        silent:true
      }
    ))
    .on('error', notify.onError({
      message: "Error: <%= error.message %>",
      title: "Error running mocha tests"
    }))
    .on('error', (er)=>{
      console.error(er.stack)
    })
));

gulp.task('lint-session', (cb) => {
  gulpSequence('cli-clear', 'lint', cb);
});

gulp.task('test-session', (cb)=> {
  gulpSequence('cli-clear', 'test', cb);
});

gulp.task('watch:lint', ['lint-session'], () => {
  gulp.watch([srcDir, testsDir], ['lint-session']);
});

gulp.task('watch:test', ()=> {
  gulp.watch([srcDir, testsDir, testsDir], ['test-session']);
});
