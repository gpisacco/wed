var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  webpack: function (config) {
    config.devtool = 'cheap-module-eval-source-map';
    //config.plugins.push(new ExtractTextPlugin("build/styles.min.css", { allChunks: true }) );

    config.module.loaders = config.module.loaders.concat([
      {
        test: /\.woff(2)?(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&mimetype=application/font-woff"
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&mimetype=application/octet-stream"
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file"
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&mimetype=image/svg+xml"
      },
      {
        test: /\.json$/,
        loader: "json"
      }
    ]);

    return config;
  }
};